# UJM_L3_S5_UE_COMPILATION

Créer un compilateur pour transcrire un programme (dont la syntaxe est choisit arbitrairement) en arbre qui sera ensuite exécuté par une machine virtuelle.

Démo : https://william-maillard.github.io/cpyrr/  

status : en construction
Détails : 
- compilateur + VM compilé en webassembly avec emscripten
- frontend react : text editor, output console, tree display
- problème : appel explicite du programme de compilation et d'exécution,
qui pour l'instant est fait au chargment du fichier wasm.

## compilateur

- analyse lexical ->  lex 
- analyse gramatical -> yacc
- analyse sémantique -> fonctions en c 
- création d'un arbre du programme à compiler dans le fichier yacc
- remplissage des tables pour enregistrer les noms, types, environnement des variables
- sauvegarde dans un fichier des tables et arbes

## machine virtuelle

- chargement tables et arbres à partir du fichier de sauvegarde
- exécution de l'abre du programme

## Installation

## Dépendances : Lex & Yacc

Lex est utilisé pour l'analyse lexicale du programme (cad segmenter le programme en token).

Yacc est utilisé pour l'analyse syntaxique du programme (cad vérifier que les tokens soient à la bonne place).

```sh
sudo apt update
sudo apt install flex
sudo apt install bison
```

## Création des exécutables

```sh
make
```
