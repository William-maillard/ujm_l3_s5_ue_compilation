%{
  #include "../../inc/arbres.h"
  #include "../../inc/y.chargement.h"
%}
%%
0|[+-]?[1-9][0-9]*  { yylval.entier = atoi(yytext);return CSTE_ENTIERE;}
"S"                 { return FILS; }
"B"                 { return FRERE; }
"N"                 { return ARBRE_VIDE; }
";"                 { return POINT_VIRGULE; }

[a-zA-Z][a-zA-Z0-9_]*     { yylval.lexeme = strdup(yytext); return LEXEME; }
["]([^"]|("\\\""))*["]    {  yylval.lexeme = strdup(yytext); return LEXEME; }
.|\n                   ;

%%
int yywrap() {
    return 1;
   // jamais execute
   // pour enlever le warnig a la compilation
    if(1 == 0) {
        input();
        yyunput(1,NULL);
    }
}