/*************************************************************
 *                                                           *
 * Fonctions permettant de manipuler la pile d'execution :   *
 *     - emplier/depiler la zone memoire d'une fct           *
 *     - trouver l'indice dans la pile d'une variable        *
 *                                                           *
 *************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "../../inc/tables.h"
#include "../../inc/pile_execution.h"
#include "../../inc/machine_virtuelle.h"

/****
 * Depile une la zone memoire de la
 * region courante
 ****/
void depiler() {
    prochaine_base = BC;
    BC = pile[BC].entier; // MAJ chainage dynamique.
    chainage_statique[pile[BC+1].entier] = BC; // MAJ chainage statique
}


/****
 * Effectue dans la pile tous les changements relatifs au
 * changement de région que sont
 *  ° Mise a jour du chainage dynamique
 *  ° Mise a jour des chainages statiques
 *  ° Changement de base courante
 ****/
void empiler(int num_region) {
    pile[prochaine_base].entier = BC; // mise en place du chainage dynamique
    BC = prochaine_base; // changement de base
    prochaine_base = BC + table_regions[num_region].taille; /* calcul de l'indice de
                                                               la prochainne BC */
    pile[BC+1].entier = table_regions[num_region].nis;
    /* mise en place du chainage statiques  = indice tab chainage_statique */
    chainage_statique[pile[BC+1].entier] = BC;
}


/****
 * Calcule le deplacement pour acceder a un champ,
 * dans une structure.
 * Entree : - num declaration de la structure
 *          - pointeur sur un noeud A_IDF representant un champ d'une structure
 * Sortie : le deplacement
 ****/
int recherche_indice_champ(int num_decl, arbre a) {
    int indice_rep, nb_champs, i;
    int deplacement;

    // indice de la structure dans la table des representation
    indice_rep = table_declarations.t[num_decl].description;

    // NB: nb_champs|num_lexico_ch1|num_dec_type_ch1|decallagech1|...
    nb_champs = table_representation.t[indice_rep];
    indice_rep++; // num lexico du 1er champs

    /* Recherche de l'indice du champs avec son numero lexico
       Quand on sort de la, on l'a forcement trouvé
       car la verif semantique a ete faite */
    i=0;
    while(i<nb_champs  &&  table_representation.t[indice_rep] != a->valeur){
        indice_rep+=3; // num_lexico champs suivant
        i++;
    }

    if(i >= nb_champs) printf("!!!!!!! PBM valeur du noeud %d indice rep %d!!!! \n", a->valeur, indice_rep);

    // Deplacement du champ dans la structure
    deplacement = table_representation.t[indice_rep+2];

    // Si le champ est une structure ou un tableau :
    a= a->frere;
    if(a != arbre_vide()) {
        if(a->nature == A_IDF) {
            return deplacement + recherche_indice_champ(table_representation.t[indice_rep+1], a);
        }
        else { // A_LISTE_INDICE
            return deplacement + recherche_indice_tableau(table_representation.t[indice_rep+1], a);
        }
    }

    // Sinon retourner le deplacement
    return deplacement;
}


/****
 * Calcule le deplacement pour acceder a une case
 * (indique par une liste d'indices) d'un tableau,
 * en partant de sa borne inferieur.
 * Entree : - num declaration du tableau
 *          - pointeur sur un noeud A_LISTE_INDICES
 * Sortie : le deplacement
 ****/
int recherche_indice_tableau(int num_decl, arbre a) {
    int deplacement= 0; //  de l'indice courant par rapport à sa borne inf
    int indice_rep, nb_dimensions;
    int borne_inf ,borne_sup;
    int taille_dimension = 1; // celle de la dimension courante
    int i;
    arbre a1;

    a1 = a->fils; // poite sur le 1 indice

    // indice du tableau dans la table des representations
    indice_rep = table_declarations.t[num_decl].description;

    // NB : nb_dimension|num_dec_type_elems|inf1|supp1|...
    nb_dimensions = table_representation.t[indice_rep];


    // on calcul le nb de cases du tableau :
    // i.e la taille du tableau / taille d'un element
    taille_dimension =  table_declarations.t[num_decl].execution /  table_declarations.t[table_representation.t[indice_rep+1]].execution;


    for(i=1; i<=nb_dimensions; i++) {
        // Recuperation de la taille de la dimension courante :
        borne_inf = table_representation.t[indice_rep+(i*2)];
        borne_sup = table_representation.t[indice_rep+(i*2+1)];
        taille_dimension /= borne_sup - borne_inf + 1;

        // on rajoute le deplacement par rapport a la dimension courante
        deplacement = deplacement + (evalue_valeur(a1).entier - borne_inf) * taille_dimension;

        // suivante
        a1 = a1->frere;
    }


    /* On prend en compte la taille du type des elements du tableau
       - on recupere le numero de declaration
       - la colonne execution de la table des declaration contient
         la taille des element du tableau.
     */
    indice_rep =  table_representation.t[indice_rep+1];
    deplacement *=  table_declarations.t[indice_rep].execution;

    // Si l'arbre courant a un frere, c'est forcement un champ
    if(a->frere != arbre_vide())
        return deplacement + recherche_indice_champ(indice_rep, a->frere);


    return deplacement;
}


/****
 * Recupere l'indice d'une variable dans
 * la pile d'execution.
 * Entree : un pointeur sur un noeud A_{MOINS_}VARIABLE
 * Sortie : l'indice dans la pile correspondant a cette
 *          variable
 ****/
int recuperer_indice_pile(arbre a) {

    int num_decl, num_region, deplacement;
    int nis_region_declaration, nis_region_utilisation;
    int base;//Indice de la base dans laquelle l'élément a été déclaré

    /***
     * Principe général de récupération de l'indice :
     *   1. On  détermine si la variable utilisée dans la région
     *       appelante a été déclarée dans la même région
     *       a. Si oui, la base courante est la base courante de la
     *           région appelante
     *
     *       b. Sinon, on recherche la base courante de la région de
     *            déclaration
     *   2. On calcule le déplacement de la variable
     *       a. Si la variable est de type simple, son déplacement
     *             est dans sa colonne exécution
     *       b. Sinon, on calcule on ajoute à son déplacement celui
     *           de son/ses champs et/ou liste d'indices
     ***/
    base = BC;

    /** 1. Variable déclarée dans la région d'utilisation ? **/
    num_region = table_declarations.t[a->valeur].region;
    // indice dans la table des chainage_statique = NIS
    nis_region_utilisation = pile[BC+1].entier;
    nis_region_declaration = table_regions[num_region].nis;

    /*
      Si NIS de la dec de la variable != NIS region courante,
      alors elle a ete declare dans une autre region.
      NB : pile_base[0] est la base courante
      et dans pile_base on stocke num_region | BC pour chaque region
    */
    if(nis_region_utilisation != nis_region_declaration) {
        // le tab contient les bases cournate des regions precedentes
        base = chainage_statique[nis_region_declaration];
    }

    /** 2. Calcul déplacement **/

    // Son déplacement se trouve dans la colonne exécution
    num_decl = a->valeur; // num_dec de la variable
    deplacement = table_declarations.t[num_decl].execution;

    // Si la variable n'est pas de type simple, recherche déplacement de ses champs/liste_indices
    a = a->fils->frere;
    if(a != arbre_vide()){

        // On recupere l'indice de la premiere structure de la variable
        num_decl = table_declarations.t[num_decl].description;

        if(a->nature == A_LISTE_INDICES) {
            deplacement += recherche_indice_tableau(num_decl,a);
        }
        else{ // A_IDF
            deplacement += recherche_indice_champ(num_decl,a);
        }
    }
    return base + deplacement;
}

/****
 * Insère une valeur dans la pile
 ****/
void inserer_valeur_pile(int indice, cellule valeur) {
    pile[indice] = valeur;
}

/****
 * Met a jour les informations de la pile
 * (cf empiler)
 * et stocke les valeurs des params dans la pile.
 * Retourne le numero de la nouvelle region courante.
 ****/
int changer_region(arbre a) {
    int num_rep_type, nb_param;
    int num_region = table_declarations.t[a->valeur].execution;
    int i;
    int deplacement; // d'un parametre

    /* 2. On insere les valeurs des params dans la pile  */
    /* table declaration colonne description =
       indice dans la table des representations */
    num_rep_type = table_declarations.t[a->valeur].description;
    nb_param = table_representation.t[num_rep_type];

    if(a->nature == A_APPEL_FCT)
        num_rep_type += 4;
    else // procedure pas de type retour
        num_rep_type += 3;

    /* a->fils : noeud A_LISTE_PARAM
       encore le fils : noeud du 1 parametre
       Les noeud des valeurs des params son frere les un des autres
    */
    a = a->fils->fils;
    //num_rep_type += 4; // deplacement du premier param
    deplacement= BC;
    // Affectation des valeurs aux éventuels paramètres
    for(i=0; i<nb_param; i++) {

        deplacement = prochaine_base + table_representation.t[num_rep_type];
        // Insertion de la valeur du paramètre à sa place dans la pile
        pile[deplacement] = evalue_valeur(a);
        printf("valeur param %d", pile[deplacement].entier);

        // eventuel param suivant
        a= a->frere;
        num_rep_type+=3; // deplacement suivant
    }


    /* 1. On met a jour les infos de la pile             */
    /* Recuperation du numero de region
       valeur du noeud : num_dec de la fct ou proc
       table declaration, colonne execution : num_region */

    empiler(num_region);
    if(flag_pile == 1) {
        printf("on a changer de region nv BC : %d, prochainne : %d\n", BC, prochaine_base);
        affiche_pile();
    }

    return num_region;
}


/****
 * Affiche la pile d'execution
 * sur le terminal.
 * Attention affiche que des entier
 * affichage indeterminer pour
 * les autres types stockes dans
 * la pile.
 ****/
void affiche_pile() {
    int i, n= pile[BC+1].entier;

    printf("chainage statique :");
    for(i=0; i<=n; i++) {
        printf(" %d", chainage_statique[i]);
    }
    printf("\n");

    printf("  i  |  pile[i]\n");
    for(i=prochaine_base - 1; i>-1; i--) {
        printf(" %3d |  %3d\n",  i, pile[i].entier);
    }
}
