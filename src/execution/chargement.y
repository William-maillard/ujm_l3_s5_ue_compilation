%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h> // pour le traitement des arguments du programme
    #include "../../inc/arbres.h"
    #include "../../inc/machine_virtuelle.h"
    #include "../../inc/tables.h"

    extern FILE * yyin;
    int yylex();
    void yyerror() {
        fprintf(stderr, "Erreur de chargement.Echec lancement execution.\n");
        exit(-1);
    }
    
    /* -- variable du YACC -- */
    int nb_regions = 0;
    int i=0;
    
%}
%start fichier_sauvegarde

%union{
    int entier;
    arbre arbre;
    char *lexeme;
}
%type<arbre> arbre fils frere
%token<entier> CSTE_ENTIERE
%token FILS FRERE ARBRE_VIDE
%token POINT_VIRGULE
%token<lexeme> LEXEME
%%

fichier_sauvegarde  : liste_arbre_regions POINT_VIRGULE tables POINT_VIRGULE

liste_arbre_regions : CSTE_ENTIERE /*le num de la region*/ arbre
                    { table_regions[$1].arbre = $2; }
                    | CSTE_ENTIERE /*le num de la region*/ arbre liste_arbre_regions
                    { table_regions[$1].arbre = $2; }
                    ;

// recursivite a droite pour constituer l'arbre de bas en haut

arbre: CSTE_ENTIERE CSTE_ENTIERE fils frere
     { $$ = concat_pere_fils(
             creer_noeud($1, $2),
             $3
             );
       $$ = concat_pere_frere($$, $4);
     }
     | ARBRE_VIDE { $$ = arbre_vide(); }
     ;

fils : FILS arbre { $$ = $2; }
     ;
frere: FRERE arbre {$$ = $2; }
     ;

// Sauvegarde des tables :
tables : table_lexico POINT_VIRGULE {i=0;}
         table_declarations POINT_VIRGULE
         CSTE_ENTIERE {table_declarations.debordement = $6; }
         POINT_VIRGULE {i=0;}
         table_representations {table_representation.indice_libre=i;}
         POINT_VIRGULE {i=0;}
         table_regions
       ;

table_lexico : CSTE_ENTIERE CSTE_ENTIERE LEXEME CSTE_ENTIERE
             { table_lexicographique.enregistrement[$1] =
                      (infos_lexicographiques) {$2, $3, $4};
             }
               table_lexico
             |
             ;

// indice nature, suivant, region, description, execution.
table_declarations : CSTE_ENTIERE CSTE_ENTIERE CSTE_ENTIERE
                     CSTE_ENTIERE CSTE_ENTIERE CSTE_ENTIERE
                   { table_declarations.t[$1] = (infos_declaration) {$2, $3, $4, $5, $6};}
                     table_declarations
                   |
                   ;

table_representations : CSTE_ENTIERE 
                      { table_representation.t[i] = $1;
                        i++;
                      }
                        table_representations
                      |
                      ;

table_regions : CSTE_ENTIERE CSTE_ENTIERE 
              { table_regions[i].taille = $1;
                  table_regions[i].nis = $2;
                  i++;
              }
                table_regions
              |
              ;

%%
int main(int argc, char **argv) {

    if(argc<2){
        printf("Donnez un fichier de sauvegarde.\n");
        printf("Donnez un 2 argument quelconque pour afficher la pile a chaque changement de region.\n");
        exit(0);
    }
    flag_pile = 0;
    if(argc == 3) {
        flag_pile=1;
    }
    
    yyin = fopen(argv[1], "r");
    if(yyin == NULL){
        fprintf(stderr,"Impossible d'ouvrir le fichier '%s'!\nVeuillez verifier que ce fichier existe ou est correctement ortographie.\nFin du programme.\n",
                argv[1]);
        exit(-1);
    }

    init_tables();// pour l'affichage
    
    printf("Debut lecture du fichier\n");
    yyparse();
    printf("Fin\n");
    //printf("Arbre de la region 0 :\n");
    //affiche_arbre(table_regions[0].arbre);

    printf("DEBUT execution :\n");
    machine_virtuelle(table_regions[0].arbre);

    if(flag_pile == 1) {
        printf("prochainne base %d, BC %d\n", prochaine_base, BC);
        printf("Voici la pile d'execution a la fin\n");
        affiche_pile();
    }
        
    // -- LIBERATION MEMOIRE --
    libere_mem_regions();

    exit(0);
}