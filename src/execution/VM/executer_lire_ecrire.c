#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../../inc/arbres.h"
#include "../../../inc/machine_virtuelle.h"
#include "../../../inc/pile_execution.h"
#include "../../../inc/tables.h"

void executer_ecrire(arbre a)
{
    arbre a_liste_variables = a->fils->frere->fils;
    char *copie = strdup(lexeme(a->fils->valeur) + 1); /* pointeur debut chaine, on fait
                                                          une copie pour ne pas modifier
                                                          la chaine stockee*/
    char *s = copie;
    char *p = s;                                   // pour parcourir la chaine
    char affichage[LONGUEUR_CONSTANTE_CHAINE_MAX]; // stocke la chaine a afficher

    *(s + strlen(s) - 1) = '\0'; // suppression du " de fin de chaine.

    while (*p != '\0')
    {

        if (*p == '\\')
        {
            /* on a trouve un caractere special, on coupe
               la chaine pour l'afficher sans le \. */
            *p = '\0'; // insere une fin de chaine
            p++;
            printf("%s", s);
            switch (*p)
            {
            case 'n': /* saut ligne     */
                printf("\n");
                break;
            case '\r': /* retour chariot */
                printf("\r");
                break;
            case 't': /* tabulation H   */
                printf("\t");
                break;
            default: /*char echape      */
                printf("\\%c", *p);
                break;
            }
            p++;   // char suivant
            s = p; // on deplace le debut de chaine
        }
        else if (*p == '%')
        {
            // le pourcent est il echape ?
            if (*(p + 1) == '%')
            {
                p += 2;
            }
            else
            {
                /* on a trouve un format, on coupe la chaine
                   pour l'afficher sans le %. */
                *p = '\0'; // insere une fin de chaine
                p++;

                // type de la valeur afficher ?
                switch (*p)
                {
                case 'd': // valeur du noeud
                    sprintf(affichage, "%s%d", s,
                            evalue_valeur(a_liste_variables).entier);
                    break;
                case 'c':
                    sprintf(affichage, "%s%c", s,
                            evalue_valeur(a_liste_variables).caractere);
                    break;
                case 'f': /* chercher la valeur dans
                             la table lexicographique */
                    sprintf(affichage, "%s%f", s,
                            evalue_valeur(a_liste_variables).reel);
                    break;
                case 's':
                    // que des constante chaine
                    sprintf(affichage, "%s%s", s, lexeme(a_liste_variables->valeur));
                    break;
                default:
                    // ne devrait pas arriver -> analyse semantique realise
                    fprintf(stderr, "Erreur format a l'execution : %%%c\n", *(p - 1));
                    affichage[0] = '\0';
                    break;
                }

                // valeur suivante :
                a_liste_variables = a_liste_variables->frere;

                /* On peut afficher la chaine */
                printf("%s", affichage);

                /* On deplace le debut de la chaine */
                p++;
                s = p;
            }
        }
        else
        { /* sinon caractere suivant */
            p++;
        }

    } // Fin boucle while.

    if (*s != '\0')
    {
        /* il reste un bout de chaine sans format a afficher */
        sprintf(affichage, "%s", s);
        printf("%s", affichage);
    }

    free(copie);
}

void executer_lire(arbre a)
{

    /* similaire a ECRIRE mais il faut evaluer
     les valeurs donnee a scanf */
    int indice, c;
    arbre a_liste_variables = a->fils->frere->fils;
    char *s = lexeme(a->fils->valeur); // pointeur debut chaine
    char *p = s;                       // pour parcourir la chaine

    while (*p != '\0')
    {
        if (*p == '\\')
            p += 2;

        // Début formattage ?
        else if (*p == '%')
        {
            if (*(p + 1) == '%')
                p += 2;
            // Si format ...
            else
            {
                // Récupération de l'indice de la variable de stockage de la valeur du scanf
                indice = recuperer_indice_pile(a_liste_variables);

                switch (*(p + 1))
                {
                    /** NB : %s n'est pas traité car le type string n'existe pas en cpyrr
                     *  Il provient d'un typedef de tableau de char
                     **/

                case 'd':
                    // Saisie de l'utilisateur
                    if (scanf("%d", &pile[indice].entier) == EOF)
                    {
                        printf("Erreur : probleme lecture!!!\n");
                        exit(-1);
                    }
                    break;

                case 'f':
                    // Saisie de l'utilisateur
                    if (scanf("%f", &pile[indice].reel) == EOF)
                    {
                        printf("Erreur : probleme lecture!!!\n");
                        exit(-1);
                    }
                    break;

                case 'c':
                    // Saisie de l'utilisateur
                    if (scanf("%c", &pile[indice].caractere) == EOF)
                    {
                        printf("Erreur : probleme lecture!!!\n");
                        exit(-1);
                    }
                    break;

                default:
                    // ne devrait pas arriver -> analyse semantique realisee
                    fprintf(stderr, "Erreur format a l'execution : %%%c\n", *(p - 1));
                    break;
                }

                // valeur suivante
                a_liste_variables = a_liste_variables->frere;
            }
        }
        p++;
    }
    // Vidage du buffer à la fin
    do
    {
        c = getchar();
    } while (c != EOF && c != '\n');
}