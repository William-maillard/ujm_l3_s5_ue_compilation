#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/arbres.h"
#include "../../../inc/machine_virtuelle.h"
#include "../../../inc/pile_execution.h"
#include "../../../inc/tables.h"

// global variables
int flag_pile; // pour opt du prog.
cellule pile[TAILLE_PILE_EXECUTION];
cellule valeur_retourne; // stocke temporairement la valeur retourne par une fonction.
int chainage_statique[NIS_MAX];
int BC;             //  L'indice de la base courante
int prochaine_base; // Lindice de la base courante si on empile une autre region

/****
 * Lance la machine virtuelle
 * Entree : arbre du programme principal
 ****/
void machine_virtuelle(arbre a)
{
    empiler(0);
    execute_arbre(a);
}

/****
 * Entree : arbre d'une instruction
 ****/
void execute_arbre(arbre a)
{
    int indice;

    if (a == arbre_vide())
        return; /* instruction vide rien a faire */

    switch (a->nature)
    {

    case A_LISTE_INSTRUCTION:
        execute_arbre(a->fils); /* instruction du noeud */
        if (a->fils != arbre_vide())
            execute_arbre(a->fils->frere); /* instruction suivante */
        break;

    case A_AFFECTATION:
        /*
          recup l'indice pile de la lvalue,
          evalue la valeur de la rvalue
          stocker le resultat obtenu dans la pile.
        */
        indice = recuperer_indice_pile(a->fils);
        pile[indice] = evalue_valeur(a->fils->frere);
        break;

    case A_TANT_QUE:
        /*
          Tant que l'evaluation de la condition (fils) renvoie vraie,
          on execute le block d'instructions de la boucle (frere du fils)
        */
        while (evalue_arbre_booleen(a->fils))
        {
            execute_arbre(a->fils->frere);
        }
        break;

    case A_INST_COND:
        /*
          Si l'evaluation de la condition renvoie vraie (fils) alors
          on execute le frere du fils,
          sinon le frere du frere du fils
         */
        if (evalue_arbre_booleen(a->fils))
            execute_arbre(a->fils->frere);
        else
            execute_arbre(a->fils->frere->frere);
        break;

    case A_RETOURNE:
        /*
          On evalue la valeur retourne (si elle existe) puis on
          la stocke dans la variable globale 'valeur_retourne'.
         */
        if (a->fils != NULL)
        {
            valeur_retourne = evalue_valeur(a->fils);
            printf("Valeur retournee : %d\n", valeur_retourne.entier);
        }
        break;

    case A_APPEL_FCT:
    case A_APPEL_PROC:
        /*
          Changer de base courante -> empiler l'indice BC
          evaluer les valeurs des arguments et les stocker.
          Executer l'abre de la fonction.
          Remette l'ancienne base courante.
        */
        printf("appel fct\n");
        indice = changer_region(a);
        execute_arbre(table_regions[indice].arbre);
        depiler();
        break;

    case A_ECRIRE:
        executer_ecrire(a);
        break;

    case A_LIRE:
        executer_lire(a);
        break;
    }
}
