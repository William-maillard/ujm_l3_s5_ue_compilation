#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/arbres.h"
#include "../../../inc/machine_virtuelle.h"
#include "../../../inc/pile_execution.h"
#include "../../../inc/tables.h"


/****
 * Evalue L'arbre d'une valeur.
 * Entree : - arbre d'une cste
 *          - arbre d'une variable
 *          - arbre d'une fonction
 *          - arbre d'une expression
 * Sortie : une cellule contenant la valeur
 ****/
cellule evalue_valeur(arbre a) {
    cellule valeur;
    int i;

    
    switch(a->nature){
        /* ----- LES CONSTANTES ----- */
    case A_CSTE_ENTIERE:
        valeur.entier = a->valeur;
        return valeur;
    case A_CSTE_REEL:
        // valeur stocke dans la table lexico
        valeur.reel = atof(lexeme(a->valeur));
        return valeur;
    case A_CSTE_CARACTERE :
        valeur.caractere = (char) a->valeur;
        return valeur;
    case A_CSTE_BOOLEEN:
        valeur.booleen = a->valeur;
        return valeur;

        
    /* ----- VARIABLES ET APPELS ----- */
        
    case A_VARIABLE:
        i = recuperer_indice_pile(a);
        return pile[i];
        
    case A_APPEL_FCT:
        execute_arbre(a);
        valeur = valeur_retourne;
        return valeur;

        
        /* ----- EXPRESSIONS ----- */
        
    case A_EXPRESSION_ARITHMETIQUE:
        if(a->valeur == NUM_DEC_ENTIER) {
            valeur.entier = evalue_arbre_int(a->fils);
        }
        else{
            valeur.reel = evalue_arbre_float(a->fils);
        }
        return valeur;
        
    case A_EXPRESSION_RELATIONNELLE :
        valeur.booleen = evalue_arbre_relationnel(a);
        return valeur;
       
    case A_EXPRESSION_BOOLEENNE:
        valeur.booleen = evalue_arbre_booleen(a->fils);
        return valeur;

    default:
        valeur.entier = 0;
        return valeur;
    }
}