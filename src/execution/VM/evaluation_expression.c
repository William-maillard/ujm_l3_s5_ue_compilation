/************************************************
 *                                              *
 * Fonctions permettant d'evaluer l'arbre       *
 * d'une expression d'un programme CPYRR :      *
 *     - arithmetique                           *
 *     - relationnelle                          *
 *     - booleenne                              *
 *                                              *
 ************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/arbres.h"
#include "../../../inc/machine_virtuelle.h"
#include "../../../inc/pile_execution.h"
#include "../../../inc/tables.h"


/****
 * Evalue l'arbre d'une expression arithm�tique sur des entiers
 ****/
int evalue_arbre_int(arbre a) {
    int diviseur;

    
    switch(a->nature) {
        
    case A_PLUS :
        return evalue_arbre_int(a->fils) + evalue_arbre_int(a->fils->frere);

    case A_MOINS :
        return evalue_arbre_int(a->fils) - evalue_arbre_int(a->fils->frere);

    case A_MULT :
        return evalue_arbre_int(a->fils) * evalue_arbre_int(a->fils->frere);

    case A_DIV :
        diviseur = evalue_arbre_int(a->fils->frere);
        if(diviseur==0) {
            fprintf(stderr,"Erreur, division par 0 impossible !!\n");
            fprintf(stderr,"Voici l'abre de l'operation illegale :\n");
            affiche_arbre(a);
            /* !!!!
               Quitte le programme;
               !!!! */
            exit(-1);
        }
        else{
            return evalue_arbre_int(a->fils) / diviseur;
        }

    case A_MOD :
        diviseur = evalue_arbre_int(a->fils->frere);
        if(diviseur==0) {
            fprintf(stderr,"Erreur, division par 0 impossible !!\n");
            fprintf(stderr,"Voici l'abre de l'operation illegale :\n");
            affiche_arbre(a);
            /* !!!!
               Quitte le programme;
               !!!! */
            exit(-1);
        }
        else{
            return evalue_arbre_int(a->fils) % diviseur;
        }

    default :
        // pas une operation, c'est une valeur.
        return evalue_valeur(a).entier;
    }
}

/****
 * Evalue l'arbre d'une expression arithm�tique sur des r�els
 ****/
float evalue_arbre_float(arbre a) {
    float diviseur;

    
     switch(a->nature) {
        
    case A_PLUS :
        return evalue_arbre_float(a->fils) + evalue_arbre_float(a->fils->frere);

    case A_MOINS :
        return evalue_arbre_float(a->fils) - evalue_arbre_float(a->fils->frere);

    case A_MULT :
        return evalue_arbre_float(a->fils) * evalue_arbre_float(a->fils->frere);
        
    case A_DIV :
        diviseur = evalue_arbre_float(a->fils->frere);
        if(diviseur==0.0){
            fprintf(stderr,"Erreur, division par 0 impossible !!\n");
            fprintf(stderr,"Voici l'abre de l'operation illegale :\n");
            affiche_arbre(a);
            /* !!!!
               Quitte le programme;
               !!!! */
            exit(-1);
        }
        else {
            return evalue_arbre_float(a->fils) / diviseur;
        }

   
     default :
         // pas une operation, c'est une valeur.
         return  evalue_valeur(a).reel;
     }
}


/****
 * Evalue l'arbre d'une expression relationnelle
 * Entree : l'arbre d'une expression relationnelle
 * Sortie : - 1 si l'exp est evalue a VRAIE
 *          - 0 sinon (FAUX)
 ****/
int evalue_arbre_relationnel(arbre a_exp) {
    arbre a= a_exp->fils; // pointe sur l'operation (<, <=, ==, ...)

    /* NB : valeur du noeud A_EXPRESSION_RELATIONNELE = num_dec des operandes */
    
    switch(a->nature) {
    case A_INF :
        if(a_exp->valeur == NUM_DEC_ENTIER)
            return evalue_valeur(a->fils).entier < evalue_valeur(a->fils->frere).entier;
        else // NUM_DEC_REEL
            return  evalue_valeur(a->fils).reel < evalue_valeur(a->fils->frere).reel;

    case A_INF_EGAL :
        if(a_exp->valeur == NUM_DEC_ENTIER)
            return evalue_valeur(a->fils).entier <= evalue_valeur(a->fils->frere).entier;
        else // NUM_DEC_REEL
            return  evalue_valeur(a->fils).reel <= evalue_valeur(a->fils->frere).reel;

    case A_SUP :
        if(a_exp->valeur == NUM_DEC_ENTIER)
            return evalue_valeur(a->fils).entier > evalue_valeur(a->fils->frere).entier;
        else // NUM_DEC_REEL
            return  evalue_valeur(a->fils).reel > evalue_valeur(a->fils->frere).reel;


    case A_SUP_EGAL :
        if(a_exp->valeur == NUM_DEC_ENTIER)
            return evalue_valeur(a->fils).entier >= evalue_valeur(a->fils->frere).entier;
        else // NUM_DEC_REEL
            return  evalue_valeur(a->fils).reel >= evalue_valeur(a->fils->frere).reel;


    case A_EGAL :
        if(a_exp->valeur == NUM_DEC_ENTIER)
            return evalue_valeur(a->fils).entier == evalue_valeur(a->fils->frere).entier;
        else // NUM_DEC_REEL
            return  evalue_valeur(a->fils).reel == evalue_valeur(a->fils->frere).reel;

    case A_DIFFERENT :
        if(a_exp->valeur == NUM_DEC_ENTIER)
            return evalue_valeur(a->fils).entier != evalue_valeur(a->fils->frere).entier;
        else // NUM_DEC_REEL
            return  evalue_valeur(a->fils).reel != evalue_valeur(a->fils->frere).reel;

        
    }
    return 0;
}

/****
 * Evalue l'arbre d'une expression bool�enne.
 * Entree : pointeur sur le noeud d'une operation booleenne
 * Sortie : 1 si vraie, 0 sinon.
 ****/
int evalue_arbre_booleen(arbre a) {

    switch(a->nature) {

    case A_OU :
        if(evalue_arbre_booleen(a->fils))
            return 1;  
        if(evalue_arbre_booleen(a->fils->frere))
            return 1;
        return 0;

        case A_ET :
            return evalue_arbre_booleen(a->fils) && evalue_arbre_booleen(a->fils->frere);

    case A_NON :
        return !(evalue_arbre_booleen(a->fils));

    case A_CSTE_BOOLEEN :
        return a->valeur;

    case A_EXPRESSION_RELATIONNELLE : 
        return evalue_arbre_relationnel(a);

    default :
        return evalue_valeur(a).entier;
        break;
    }
    return 0;
}