%{
    /*-----------------------------------------------------------------------------------*
     *                                                                                   *
     * Grammaire du langage CPYRR, les règles de grammaires sont dans l'ordre suivant :  *
     *   - Definition d'un programme CPYRR                                               *
     *   - Les declarations                                                              *
     *       . variables                                                                 *
     *       . types                                                                     *
     *       . procedures & fonctions                                                    *
     *   - Les instructions                                                              *
     *   - Les modifieurs de flux                                                        *
     *   - variables & affectation                                                       *
     *   - Les expressions                                                               *
     *       . arithmetiques                                                             *
     *       . booleennes                                                                *
     *       . relationnelles                                                            *
     *   - Les appels de fonctions & procedure                                           *
     *   - Les entrees & sorties                                                         *
     *                                                                                   *
     *-----------------------------------------------------------------------------------*/
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h> // pour le traitement des arguments du programme
    #include "../../inc/analyse_semantique.h"
    #include "../../inc/arbres.h"
    #include "../../inc/erreur.h"
    #include "../../inc/pile_region.h"
    #include "../../inc/sauvegarde.h"
    #include "../../inc/tables.h"
    #include "../../inc/machine_virtuelle.h"
    
    /* -- variables gerees par le LEX --*/
    extern FILE * yyin;
    extern int yyleng;
    extern char *yytext;
    int num_ligne= 1; // numero de la ligne courante
    int pos_ligne; // position dans la ligne courante
    char *buffer_ligne; // contient la ligne courante

    /* -- variable du YACC -- */
    int nb_regions = 0;
    int tmps;

    
    int yylex(); 

    /* -- Pour la gestion des erreurs -- */    
    void affiche_ligne_erreur(){
        fprintf(stderr,"Erreur ligne %d :\n%s\n", num_ligne, buffer_ligne);        
    }
    
    void yyerror(){
        affiche_ligne_erreur();
        fprintf(stderr,"%*s\n", pos_ligne, "^");
        
        //fprintf(stdout, "(yacc, yyerror) yyleng = %d; yytext=%s\n\n", yyleng, yytext);
    }
%}
%start programme

%union{
    int entier;
    char *chaine;
    arbre arbre;
 }

/* ----- Definition des types des symboles non terminaux ----- */
%type<entier> nom_type type_simple un_champs

%type<arbre> block_instructions liste_instructions une_instruction
%type<arbre> modifieur_conditionnel conditions fin_si modifieur_boucle_tq
%type<arbre> expression expression_relationnelle
%type<arbre> expression_arithmetique e2 e3
%type<arbre> expression_booleenne b2 b3 
%type<arbre> variable liste_indices
%type<arbre> affectation resultat_retourne une_valeur
%type<arbre> appel parametres liste_arguments un_argument
%type<arbre> sortie entree liste_variables chaine_formatee


/* ------ Definition des symboles terminaux et leur type eventuel -----*/
%token PROG
%token DEBUT FIN
%token RETOURNE
%token<entier> IDF
%token LIRE ECRIRE


%token TYPEDEF STRUCT
%token VARIABLE
%token TABLEAU DE
%token PROCEDURE FONCTION


%token VIDE ENTIER REEL BOOLEEN CARACTERE
%token<entier> CSTE_ENTIERE CSTE_BOOLEEN CSTE_CARACTERE CSTE_REEL CSTE_CHAINE

%token OPAFF
%token PLUS MOINS MULT DIV MOD


%token EGAL DIFFERENT
%token SUPERIEUR SUP_EGAL
%token INFERIEUR INF_EGAL
%token OU ET NON


%token TANT_QUE
%token SI ALORS SINON


%token VIRGULE POINT_VIRGULE
%token POINT POINT_POINT DEUX_POINTS
%token PARENTHESE_OUVRANTE PARENTHESE_FERMANTE
%token CROCHET_OUVRANT CROCHET_FERMANT
%token ACCOLLADE_OUVRANTE ACCOLLADE_FERMANTE
 

%%
/* ---------------------------------------------------------------------------
  |                                                                           |
  |                    Definition d'un programme CPYRR                        |
  |                                                                           |
   --------------------------------------------------------------------------- */
programme : PROG corps
          ;

corps : ACCOLLADE_OUVRANTE liste_declarations block_instructions ACCOLLADE_FERMANTE
      { if(semantique_corps_fonction($3) == CODE_ERREUR){
            nb_erreurs++;
        }
        inserer_arbre_region($3);
      }
      ;


/* -------------------------------------------------------------------------
  |                                                                         |
  |                             Les declarations                            |
  |                                                                         |
   ------------------------------------------------------------------------- */
liste_declarations : 
                   | liste_declarations declaration POINT_VIRGULE
                   | error POINT_VIRGULE
                   ;

declaration        : declaration_variable
                   | declaration_type
                   | declaration_procedure
                   | declaration_fonction
                   ;


/* ---------------- DECLARATION  DE VARIABLES  ----------------- */

declaration_variable : VARIABLE IDF { $2= inserer_dec(TYPE_VARIABLE, $2);}
                       DEUX_POINTS nom_type
                       { if($2 == -1){
                               affiche_ligne_erreur();
                               nb_erreurs++;
                         }else{
                               completer_dec_variable($2, $5);
                         }
                       }
                     ;

nom_type             : type_simple {$$ = $1;}
                     | IDF
                     { // cherche une dec de struct ou tableau
                       $$ = association_nom($1, TYPE_STRUCTURE);
                       if($$ == -1){
                             fprintf(stderr, "Erreur le lexeme '%s' utilise en ligne %d ne correspond a aucune declaration accesible depuis la region courante.\n"
                                     ,lexeme($1), num_ligne);
                             nb_erreurs++;
                         }
                     }
                     ;

type_simple          : VIDE      {$$ = NUM_DEC_VIDE;}
                     | ENTIER    {$$ = NUM_DEC_ENTIER;}
                     | REEL      {$$ = NUM_DEC_REEL;}
                     | BOOLEEN   {$$ = NUM_DEC_BOOLEEN;}
                     | CARACTERE {$$ = NUM_DEC_CARACTERE;}
                     ;


/* ------------------ DECLARATION DE TYPES  ------------------ */

declaration_type       : TYPEDEF IDF {tmps= $2;} DEUX_POINTS suite_declaration_type
                       ;

// on stocke le num de declaration dans tmps.
suite_declaration_type : STRUCT {tmps= inserer_dec(TYPE_STRUCTURE, tmps);
                         if(tmps == -1){
                                   affiche_ligne_erreur();
                                   nb_erreurs++;
                               }
                       }
                         ACCOLLADE_OUVRANTE liste_champs ACCOLLADE_FERMANTE
                       | TABLEAU {tmps= inserer_dec(TYPE_TABLEAU, tmps);
                          if(tmps == -1){
                                   affiche_ligne_erreur();
                                   nb_erreurs++;
                               }
                         }
                         dimension DE nom_type
                       { inserer_type_taille_tableau(tmps,$5); }
                       ;

                       /* specifique aux tableaux */
dimension              : CROCHET_OUVRANT liste_dimensions CROCHET_FERMANT
                       ;

liste_dimensions       : une_dimension 
                       | liste_dimensions VIRGULE une_dimension
                       ;

une_dimension          : CSTE_ENTIERE POINT_POINT CSTE_ENTIERE
                       { ajouter_dimension($1, $3, tmps);}
                       ;

                       /* specifique aux structures */
liste_champs           : un_champs POINT_VIRGULE
                       | liste_champs  un_champs POINT_VIRGULE
                       ;

un_champs              : IDF DEUX_POINTS nom_type
                       { if(ajouter_champs($1, $3, tmps) == -1) {
                               affiche_ligne_erreur();
                               nb_erreurs++;
                         }
                       }
                       ;


/* -------------- DECLARATION DE PROCEDURE & FONCTION  --------------- */
// on va stocker dans temps l'indice de la table des representations
declaration_procedure        : PROCEDURE IDF
                             { nb_regions++; empiler_region(nb_regions); inserer_region();
                               tmps= inserer_dec(TYPE_PROCEDURE, $2);
                               if(tmps == -1){
                                   affiche_ligne_erreur();
                                   nb_erreurs++;
                               }
                               tmps = table_declarations.t[tmps].description;
                             }
                               declaration_parametres corps
                               { depiler_region(); }
                             ;

declaration_fonction         : FONCTION IDF
                             { nb_regions++; empiler_region(nb_regions); inserer_region();
                                tmps = inserer_dec(TYPE_FONCTION, $2);
                                if(tmps == -1){
                                   affiche_ligne_erreur();
                                   nb_erreurs++;
                                }
                                tmps = table_declarations.t[tmps].description;
                             }
                               declaration_parametres RETOURNE type_simple
                             { inserer_type_retour(tmps, $6);
                               empiler_type_retour($6);
                             }
                               corps
                             { depiler_region(); }
                             ;

declaration_parametres       : PARENTHESE_OUVRANTE PARENTHESE_FERMANTE
                             | PARENTHESE_OUVRANTE liste_declaration_parametres PARENTHESE_FERMANTE
                             ;

liste_declaration_parametres : declaration_d_un_parametre
                             | liste_declaration_parametres VIRGULE  declaration_d_un_parametre
                             ;

declaration_d_un_parametre   : IDF DEUX_POINTS type_simple
                             { ajouter_parametre(tmps, $1, $3); }
                             ;


/* ---------------------------------------------------------------------------
  |                                                                           |
  |                           Les instructions                                |
  |                                                                           |
   --------------------------------------------------------------------------- */

block_instructions : DEBUT liste_instructions FIN { $$ = $2; }
                   | DEBUT FIN { $$ = creer_noeud(A_LISTE_INSTRUCTION,-1); }
                   ;

liste_instructions : une_instruction
                   { $$ = concat_pere_fils(creer_noeud(A_LISTE_INSTRUCTION,-1), $1); }
                   | une_instruction liste_instructions 
                   { $$ = concat_pere_fils(
                           creer_noeud(A_LISTE_INSTRUCTION,-1),
                           concat_pere_frere($1,$2)
                           );
                   }
                   ;

une_instruction    : affectation { if(semantique_affectation($1)==CODE_ERREUR) {nb_erreurs++;} } POINT_VIRGULE { $$ = $1; }
                   | modifieur_conditionnel             { $$ = $1; }
                   | modifieur_boucle_tq                { $$ = $1; }
                   | RETOURNE resultat_retourne { if(semantique_retourne($2)==CODE_ERREUR){nb_erreurs++;} } POINT_VIRGULE
                   { $$ = concat_pere_fils(
                           creer_noeud(A_RETOURNE, type_retour()),
                           $2);
                   } 
                   | appel POINT_VIRGULE { $$ = $1; }
                   | entree POINT_VIRGULE { $$ = $1; }
                   | sortie POINT_VIRGULE { $$ = $1; }
                   | POINT_VIRGULE
                   { $$ = creer_noeud(A_LISTE_INSTRUCTION, -1); }
                   | error POINT_VIRGULE
                   {/* si une erreur survient, on shift jusqu'au prochain ;*/
                     $$ = creer_noeud(A_LISTE_INSTRUCTION, -1);
                     nb_erreurs++;
                   }
                   ;

resultat_retourne  :             { $$ = arbre_vide(); }
                   | une_valeur  { $$ = $1; }
                   ;


/* ---------------------------------------------------------------------------
  |                                                                           |
  |                           Les modifieurs de flux                          |
  |                                                                           |
   --------------------------------------------------------------------------- */

modifieur_conditionnel : SI conditions {if(semantique_expression_booleenne($2)==CODE_ERREUR) {nb_erreurs++;} } ALORS block_instructions fin_si
                       { $$= concat_pere_fils(
                                creer_noeud(A_INST_COND,-1),
                                concat_pere_frere($2,concat_pere_frere($5,$6))
                                );
                       }
                       ;

fin_si                 :                               { $$ = arbre_vide(); }
                       | SINON block_instructions      { $$ = $2; }
                       | SINON modifieur_conditionnel  { $$ = $2; }
                       ;

modifieur_boucle_tq    : TANT_QUE conditions {semantique_expression_booleenne($2); } block_instructions
                       { $$ = concat_pere_fils(
                               creer_noeud(A_TANT_QUE,-1),
                               concat_pere_frere($2,$4)
                               );
                           //block_instruction stocke l'arbre de la liste
                       }
                       ;

conditions             : PARENTHESE_OUVRANTE expression PARENTHESE_FERMANTE { $$=$2; }
                       ;


/* ---------------------------------------------------------------------------
  |                                                                           |
  |                          Variables & Affectations                         |
  |                                                                           |
   --------------------------------------------------------------------------- */

affectation : variable OPAFF une_valeur
            {
                $1 = concat_pere_fils(
                    creer_noeud(A_VARIABLE, -1),
                    $1);
                $$ = concat_pere_fils(
                    creer_noeud(A_AFFECTATION, -1),
                    concat_pere_frere($1,$3)
                    );
            }
            ;

une_valeur  : CSTE_CARACTERE // contient son code ASCII
            { $$ = creer_noeud(A_CSTE_CARACTERE,$1); }
            | CSTE_CHAINE // contient son num_lexico
            { $$ = creer_noeud(A_CSTE_CHAINE,$1); }
            | expression     { $$ = $1; }
            ;

variable    : IDF
            { $$ = creer_noeud(A_IDF, $1); }
            | IDF CROCHET_OUVRANT  liste_indices CROCHET_FERMANT
            {
                $3 = concat_pere_fils(creer_noeud(A_LISTE_INDICES,-1), $3);
                $$ = concat_pere_frere(creer_noeud(A_IDF, $1), $3);
            }
            | IDF POINT variable
            { $$ = concat_pere_frere(
                    creer_noeud(A_IDF,$1),
                    $3);
            }
            | IDF CROCHET_OUVRANT  liste_indices CROCHET_FERMANT POINT variable
            {   
                $3 = concat_pere_fils(creer_noeud(A_LISTE_INDICES,-1), $3);
                $$ = concat_pere_frere(
                    creer_noeud(A_IDF,$1),
                    concat_pere_frere($3, $6));
            }
            ;

/* NB : on met expression bien que seul les expressions arithmetiques
sont concernees, car cela permet d'avoir un noeud A_EXPRESSION_ARITHMETIQUE
dans l'arbre, necessaire pour l'analyse semantique */
liste_indices : expression
              { $$ = $1; }
              | expression VIRGULE liste_indices
              { $$ = concat_pere_frere($1, $3); }
              ;


/* ---------------------------------------------------------------------------
  |                                                                           |
  |                              Les expressions                              |
  |                                                                           |
   --------------------------------------------------------------------------- */
expression : expression_arithmetique
           { switch($1->nature){
               case A_PLUS:
               case A_MOINS:
               case A_MULT:
               case A_DIV:
               case A_MOD:
                   $$ = concat_pere_fils(
                       creer_noeud(A_EXPRESSION_ARITHMETIQUE, -1),
                       $1);
                   break;
               default:
                   $$ = $1;
             }
           }
           | expression_booleenne
           {
               if($1->nature == A_CSTE_BOOLEEN) {
                   $$ = $1;
               }else{
                   $$ = concat_pere_fils(
                       creer_noeud(A_EXPRESSION_BOOLEENNE, -1),
                       $1);
               }
           }
           | expression_relationnelle
           {
               $$ = concat_pere_fils(
                   creer_noeud(A_EXPRESSION_RELATIONNELLE, -1),
                   $1);
           }
           ;

/* ---------------------- Les expressions arithmetiques --------------------- */
/*
 * Priorite :
 *  ()
 *  * / %
 *  + -
 *****************/
expression_arithmetique : expression_arithmetique PLUS e2
                        { $$ = concat_pere_fils(
                                creer_noeud(A_PLUS,-1),
                                concat_pere_frere($1,$3)
                                );
                        }
                        | expression_arithmetique MOINS e2
                        { $$ = concat_pere_fils(
                                creer_noeud(A_MOINS,-1),
                                concat_pere_frere($1,$3)
                                );
                        }
                        | e2
                        { $$ = $1; }
                        ;

e2                      : e2 MULT e3
                        { $$ = concat_pere_fils(
                                creer_noeud(A_MULT,-1),
                                concat_pere_frere($1,$3)
                                );
                        }
                        | e2 DIV e3  /* attention si entier ou float */
                        { $$ = concat_pere_fils(
                                creer_noeud(A_DIV,-1),
                                concat_pere_frere($1,$3)
                                );
                        }
                        | e2 MOD e3  /* attention que pour les entiers */
                        { $$ = concat_pere_fils(
                                creer_noeud(A_MOD,-1),
                                concat_pere_frere($1,$3)
                                );
                        }
                        | e3
                        { $$ = $1; }
                        ;

e3                      : PARENTHESE_OUVRANTE expression_arithmetique PARENTHESE_FERMANTE
                        { $$ = $2; }
                        | CSTE_ENTIERE   { $$ = creer_noeud(A_CSTE_ENTIERE,$1); }
                        | CSTE_REEL      {$$ = creer_noeud(A_CSTE_REEL,$1);}
                        //Sa valeur est le numero lexico de la chaine la representant
                        | variable
                        { $$ = concat_pere_fils( creer_noeud(A_VARIABLE, -1), $1); }
                        | MOINS variable
                        { $$ = concat_pere_fils(
                                creer_noeud(A_MOINS,-1),
                                concat_pere_frere(
                                    creer_noeud(A_CSTE_ENTIERE, 0),
                                    concat_pere_fils(creer_noeud(A_VARIABLE, -1), $2)
                                    )
                                );
                        }
                        | appel { $$ = $1; }
                        ;

/* ------------------------ Les expressions booleennes ---------------------- */
/*
 * Priorite :
 *  ()
 *  < <= > >= ==
 *  !
 *  && ||
 *****************/
expression_booleenne : expression OU b2
                     { $$= concat_pere_fils(
                             creer_noeud(A_OU,-1),
                             concat_pere_frere($1,$3)
                             );
                     }
                     | expression OU b3
                     { $$= concat_pere_fils(
                             creer_noeud(A_OU,-1),
                             concat_pere_frere($1,$3)
                             );
                     }
                     | expression ET b2
                     { $$= concat_pere_fils(
                             creer_noeud(A_ET,-1),
                             concat_pere_frere($1,$3)
                             );
                     }
                     | expression ET b3
                     { $$= concat_pere_fils(
                             creer_noeud(A_ET,-1),
                             concat_pere_frere($1,$3)
                             );
                     }
                     | b2
                     { $$ = $1; }
                     ;

b2                   : NON b3       { $$= concat_pere_fils(creer_noeud(A_NON,-1),$2); }
                     | CSTE_BOOLEEN { $$= creer_noeud(A_CSTE_BOOLEEN,$1); }
                     ;

b3                   : PARENTHESE_OUVRANTE expression_booleenne PARENTHESE_FERMANTE
                     { $$ = $2; }
                     | expression_relationnelle
                     { $$ = concat_pere_fils(creer_noeud(A_EXPRESSION_RELATIONNELLE,-1),
                                             $1);
                     }
                     | expression_arithmetique
                     { concat_pere_fils(creer_noeud(A_EXPRESSION_ARITHMETIQUE,-1), $1); }
                     ;

/* ---------------------- Les expressions relationnelles --------------------- */

expression_relationnelle : expression_arithmetique INFERIEUR expression_arithmetique
                         { $$=concat_pere_fils(
                                creer_noeud(A_INF,-1),
                                concat_pere_frere($1,$3)
                                );
                         }
                         | expression_arithmetique INF_EGAL expression_arithmetique
                         { $$=concat_pere_fils(
                                 creer_noeud(A_INF_EGAL,-1),
                                 concat_pere_frere($1,$3)
                                 );
                         }
                         | expression_arithmetique SUPERIEUR expression_arithmetique
                         { $$=concat_pere_fils(
                                 creer_noeud(A_SUP,-1),
                                 concat_pere_frere($1,$3)
                                 );
                         }
                         | expression_arithmetique SUP_EGAL expression_arithmetique
                         { $$=concat_pere_fils(
                                 creer_noeud(A_SUP_EGAL,-1),
                                 concat_pere_frere($1,$3)
                                 );
                         }
                         | expression_arithmetique EGAL expression_arithmetique
                         { $$=concat_pere_fils(
                                 creer_noeud(A_EGAL,-1),
                                 concat_pere_frere($1,$3)
                                 );
                         }
                         | expression_arithmetique DIFFERENT expression_arithmetique
                         { $$=concat_pere_fils(
                                 creer_noeud(A_DIFFERENT,-1),
                                 concat_pere_frere($1,$3)
                                 );
                         }
                         | PARENTHESE_OUVRANTE expression_relationnelle PARENTHESE_FERMANTE
                         { $$ = $2; }


/* ---------------------------------------------------------------------------
  |                                                                           |
  |                  Les appels de fonctions & procedures                     |
  |                                                                           |
   --------------------------------------------------------------------------- */

appel           : IDF parametres
                { // cherche aussi les procedure en meme temps
                    tmps = association_nom($1,TYPE_FONCTION);
                    if(tmps != -1) {
                        if(table_declarations.t[tmps].nature == TYPE_FONCTION){
                            $$ = concat_pere_fils(
                                creer_noeud(A_APPEL_FCT, tmps),
                                $2
                                );
                        } else {
                            $$ = concat_pere_fils(
                                creer_noeud(A_APPEL_PROC, tmps),
                                $2
                                );
                        }
                        
                        
                        if(semantique_appel($$) == CODE_ERREUR) {
                            nb_erreurs++;
                        }
                    }
                    else {
                        fprintf(stderr, "La procedure ou fonction %s appelee a la ligne %d n'existe pas dans le programme.\n",
                                lexeme($1), num_ligne);
                        nb_erreurs++;
                        $$ = creer_noeud(A_LISTE_INSTRUCTION, -1); 
                    }
                }
                ;

parametres      : PARENTHESE_OUVRANTE liste_arguments PARENTHESE_FERMANTE
                { $$ = concat_pere_fils(
                        creer_noeud(A_LISTE_PARAM,-1),
                        $2 );
                }
                | PARENTHESE_OUVRANTE PARENTHESE_FERMANTE
                { $$ = creer_noeud(A_LISTE_PARAM,-1); }
                ;

liste_arguments : un_argument
                { $$ = $1; }
                | un_argument VIRGULE liste_arguments
                { $$ = concat_pere_frere($1, $3); }
                ;

un_argument     : une_valeur { $$ = $1; }
                ;


/* ---------------------------------------------------------------------------
  |                                                                           |
  |                         Les entrees sorties                               |
  |                                                                           |
   --------------------------------------------------------------------------- */
                  // lit des valeurs de type simple
entree          : LIRE chaine_formatee
                { $$ = concat_pere_fils(creer_noeud(A_LIRE, -1),
                                        $2);
                    if(semantique_chaine_formatee($$)==CODE_ERREUR)
                    {nb_erreurs++;}
                } 
                ;

                /* affiche une chaine formatee comme en C */
sortie          : ECRIRE  chaine_formatee
                { $$ = concat_pere_fils(creer_noeud(A_ECRIRE, -1), 
                                          $2);
                    if(semantique_chaine_formatee($$)==CODE_ERREUR)
                    {nb_erreurs++;}
                } 
                ;

chaine_formatee : PARENTHESE_OUVRANTE CSTE_CHAINE liste_variables PARENTHESE_FERMANTE
                { $$= concat_pere_frere(
                        creer_noeud(A_CSTE_CHAINE, $2),
                        concat_pere_fils(creer_noeud(A_LISTE_VARIABLES,-1) ,$3)
                        );
                }
                ;

liste_variables : { $$ = arbre_vide(); }
                | VIRGULE une_valeur liste_variables
                { $$ = concat_pere_frere($2, $3); }
                ;


%%
/****
 * Fonction qui affiche l'aide du programme sur le terminal
 ****/
void usage(char *s) {
    printf("usage %s : <options> fichier.cpyrr\n", s);
    printf("<options> :\n");
    printf("\t-h : afficher cette aide\n");
    printf("\t-t : afficher les tables apres la compilation\n");
    printf("\t-a : afficher l'abre du programme, prend un argument optionnel : le numero de region de l'arbre a afficher, -1 si tous.\n");
    printf("\t-c : enregistre les arbres et tables dans un fichier .opyrr du meme nom pour une execution ulterieure.\n");

    // a completer lors d'ajout d'options

    printf("\n");
    exit(0);
}
                
                
int main(int argc, char **argv) {
    char opt;
    int flag = 1;
    char *chemin;
    FILE *f;
    int n=0, region=0;


    // -- TRAITEMENT DES ARGUMENTS DU PROGRAMME --

    // pour l'instant que les options de la forme '-c'
    while((opt = getopt(argc, argv, "+:ta::hc")) != -1) {
        switch(opt) {
        case 't': flag *= 2; break;
        case 'a': flag *= 3; if(optarg != NULL)region = atoi(optarg); break;
        case 'c': flag *= 5; break;
        case 'h': usage(argv[0]); break;
        default: printf("Option %c inconnu.\n", opt);
        }
    }

    if(optind < argc){
        yyin = fopen(argv[optind], "r");
        if(yyin == NULL){
            fprintf(stderr,"Impossible d'ouvrir le fichier '%s'!\nVeuillez verifier que ce fichier existe ou est correctement ortographie.\nFin du programme.\n",
                    argv[optind]);
            exit(-1);
        }
    }
    else{
        fprintf(stderr, "Vous n'avez pas donne de fichier cpyrr a compiler!\nFin du programme.\nPour optenir de l'aide, utiliser l'option '-h'\n");
        exit(-1);
    }
    

    
    // -- INITIALISATIONS --
    buffer_ligne = (char*)  malloc(500*sizeof(char));
    init_tables();
    init_pile_region();
    nb_erreurs= 0;
    inserer_region();

    // -- LANCEMENT DE L'ANALYSEUR --
    fprintf(stderr,"Lancement de la compilation :\n");
    yyparse();
    fprintf(stderr,"Fin de la compilation.\n");

    // -- AFFICHAGE -- (selon les option utilisateurs)
    if(flag%2 == 0){
        affiche_tables();
    }
    if(flag%3 == 0){
        if(region <= nb_regions) {
            if(region > -1) {
                n = region; // que une region a afficher.
            }
            else{
                region = 0;
                n = nb_regions;
            }
            for(; region<=n; region++) {
                printf("==== ARBRE REGION %d =====\n\n", region);
                affiche_arbre(table_regions[region].arbre);
                printf("\n=============================\n\n");   
            }
        }
        else{
            printf("Erreur d'argument, pas de region %d a afficher.\n", region);
        }
    }

    
    // -- SAUVEGARDE OU EXECUTION CELON OPT UTIL --
    if(nb_erreurs == 0){
        if(flag%5 == 0) {
            chemin = strdup(argv[optind]);
            chemin[strlen(chemin) -5] = 'o';
            printf("fichier de sauvegarde : %s\n", chemin);
            f = fopen(chemin, "w");
            free(chemin);
            if(f != NULL){
                sauvegarde_programme(f);
                printf("Sauvegarde effectue.\n");
                fclose(f);
            }
            else{
                printf("Echec ouverture fichier.\n");
            }
        }
        else{
            // On execute l'arbre du programme principal
            machine_virtuelle(table_regions[0].arbre);
        }
    }
    else{
        if(flag%5 == 0)
            printf("Pas de sauvegarde, car des erreurs.\n");
        else
            printf("Pas d'execution, car des erreurs.\n");
    }

    
    // -- LIBERATION MEMOIRE --
    libere_mem_regions();
    free(buffer_ligne);

    exit(0);
}