/*****************************************************************
 *                                                               *
 * Définition de la pile des regions utilise pour la compilation *
 * et l execution d un programme CPYRR.                          *
 * Et des fonctions permettant de manipuler cette pile           *
 * Pour chaque region on a :                                     *
 *   - le num de declaration du type retourne                    *
 *   - le numero de la region                                    *
 *                                                               *
 **************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include "../../inc/pile_region.h"
#include "../../inc/tables.h"


/* *** Initialise la pile *** */
void init_pile_region(){
    pile_region[1] =  NUM_DEC_VIDE; // type de retour
    pile_region[2] = 0; // region principale
    pile_region[0] = 2; // augm la taille
}

/* *** renvoie le nis de la region en sommet de pile *** */
int nis_region_courante(){
    return (pile_region[0]-2) / 2;
}

/* *** renvoie le numero de la region courante *** */
int region_courante(){
    return pile_region[pile_region[0]];
}

/* ***
 * renvoie le numero de declaration
 * du type retourne par la region courante
 *** */
int type_retour() {
    return pile_region[pile_region[0]-1];
}

/* *** sort de la region courante *** */
void depiler_region(){
    pile_region[0]-= 2;
}

/* *** region devient la region courante *** */
void empiler_region(int region){
    pile_region[0]+=2;  
    // type retour par defaut
    pile_region[pile_region[0]-1] = NUM_DEC_VIDE;
    pile_region[pile_region[0]] = region;
}

/* *** ajout un type de retour a la region courante *** */
void empiler_type_retour(int type_retour) {
    pile_region[pile_region[0]-1] = type_retour;
}

/* *** 
 * cherche a quel niveau se situe region par rapport a la region courante.
 * retourne -1 si region n'est pas une region qui englobe la region courante
 * son niveau d'englobement sinon.
 * (ex si region = region precedente de la region courante -> 1
 *          region precedent la precedente ______________  -> 2
 * ...) 
 *** */
int region_englobante(int region){
    int n=pile_region[0], i=n;

    while(pile_region[i] != region  &&  (i-=2) > 0)
        ;
    if(i == 0)
        return -1;
    
    return n-i;
}


/* *** Renvoie le numero de la i° region englobante *** */
int num_region_englobante(int i) {
    return pile_region[pile_region[0] - 2*i];
}

/* *** affiche la pile des regions sur le terminal *** */
void affiche_pile_region(){
    int n=pile_region[0], i;
    
    printf("-- PILE DES REGIONS (taille %2d)--\n", pile_region[0]/2);
    for(i=n; i>0; i-=2)
    {
        printf("| region : %2d, type retour : ", pile_region[i]);
        switch(pile_region[i-1]) {
        case NUM_DEC_VIDE:
            printf("vide |\n");
            break;
        case NUM_DEC_ENTIER:
            printf("int  |\n");
            break;
        case NUM_DEC_REEL:
            printf("reel |\n");
            break;
        case NUM_DEC_CARACTERE:
            printf("char |\n");
            break;
        case NUM_DEC_BOOLEEN:
            printf("bool |\n");
        default:
            printf("???? |\n");
        }
    }
}
