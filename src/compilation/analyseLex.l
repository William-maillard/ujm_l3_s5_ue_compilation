%{
  #include <string.h>
  #include "../../inc/tables.h"
  #include "../../inc/y.tab.h"

  /* -- Pour la gestion des erreurs -- */
  #define NEW_LINE num_ligne++; pos_ligne=0;
  #define INC_POS pos_ligne += yyleng

  extern char *buffer_ligne; // stocke la ligne en cours de lecture
  extern int num_ligne;
  extern int pos_ligne; // stoke la position dans la ligne

 // NB la variable yylval permet d'associer une valeur ($i) au token renvoyé
%}
%%

 /* -- On sauvegarde la ligne courante dans une chaine --*/
"\r" ; // pour les fichiers windows.

\n.*                    {NEW_LINE
                         strcpy(buffer_ligne, yytext+1);
                         //plus 1 pour ne pas prendre le \n
                         yyless(1); /* rescanner la ligne sans le \n*/}


PROG                     {INC_POS; return PROG;}
begin                    {INC_POS; return DEBUT;}
end                      {INC_POS; return FIN;}
return                   {INC_POS; return RETOURNE;}
scanf                    {INC_POS; return LIRE;}
printf                   {INC_POS; return ECRIRE;}


typedef                  {INC_POS; return TYPEDEF;}
array                    {INC_POS; return TABLEAU;}
of                       {INC_POS; return DE;}
struct                   {INC_POS; return STRUCT;}
procedure                {INC_POS; return PROCEDURE;}
function                 {INC_POS; return FONCTION;}
var                      {INC_POS; return VARIABLE;}


int                      {INC_POS; return ENTIER;}
float                    {INC_POS; return REEL;}
bool                     {INC_POS; return BOOLEEN;}
char                     {INC_POS; return CARACTERE;}
void                     {INC_POS; return VIDE;}


0|[+-]?[1-9][0-9]*                                        {INC_POS; yylval.entier = atoi(yytext);return CSTE_ENTIERE;}
[+-]?(0|([1-9][0-9]*))"."[0-9]*([eE][+-]?[1-9][0-9]*)?    {INC_POS; yylval.entier = inserer_lexico(yytext); return CSTE_REEL;}
"."[0-9]+([eE][+-]?[1-9][0-9]*)?                          {INC_POS; yylval.entier = inserer_lexico(yytext); return CSTE_REEL;}
(0|[+-]?[1-9][0-9]*)/..                                   {INC_POS; pos_ligne-= 2; yylval.entier = atoi(yytext);return CSTE_ENTIERE;}
true                                                      { yylval.entier= 1;
                                                            return CSTE_BOOLEEN; }
false                                                     { yylval.entier= 0;
                                                            return CSTE_BOOLEEN; }
[']([^']|[\'])[']       { INC_POS; yylval.entier = (int)yytext[1]; return CSTE_CARACTERE;}
["]([^"]|("\\\""))*["]                                      { INC_POS;
                                                            // On verifie que la chaine n'est pas trop longue
                                                            if(strlen(yytext) > LONGUEUR_CONSTANTE_CHAINE_MAX){
                                                                fprintf(stderr, "Warning, la chaine constante en ligne %d est trop longue (> %d caracteres).\n",
                                                                            num_ligne, LONGUEUR_CONSTANTE_CHAINE_MAX);
                                                                 fprintf(stderr, " Elle va etre tronque lors de l'insertion dans la table, ce qui va modifier l'affichage final du programme.\n");
                                                                 *(yytext+LONGUEUR_CONSTANTE_CHAINE_MAX+1) = '\0';
                                                            }
                                                            //insertion et recuperation du numero lexicographique
                                                            yylval.entier = inserer_lexico(yytext);
                                                            return CSTE_CHAINE;
                                                           }


:=                       {INC_POS; return OPAFF;}
[+]                      {INC_POS; return PLUS;}
[-]                      {INC_POS; return MOINS;}
[*]                      {INC_POS; return MULT;}
[/]                      {INC_POS; return DIV;}
[%]                      {INC_POS; return MOD;}


"=="                     {INC_POS; return EGAL;}
"!="                     {INC_POS; return DIFFERENT;}
">"                      {INC_POS; return SUPERIEUR;}
">="                     {INC_POS; return SUP_EGAL;}
"<"                      {INC_POS; return INFERIEUR;}
"<="                     {INC_POS; return INF_EGAL;}
"||"                     {INC_POS; return OU;}
"&&"                     {INC_POS; return ET;}
"!"                      {INC_POS; return NON;}


while                    {INC_POS; return TANT_QUE;}
if                       {INC_POS; return SI;}
then                     {INC_POS; return ALORS;}
else                     {INC_POS; return SINON;}


";"                      {INC_POS; return POINT_VIRGULE;}
","                      {INC_POS; return VIRGULE;}
"."                      {INC_POS; return POINT;}
".."                     {INC_POS; return POINT_POINT;}
":"                      {INC_POS; return DEUX_POINTS;}
"("                      {INC_POS; return PARENTHESE_OUVRANTE;}
")"                      {INC_POS; return PARENTHESE_FERMANTE;}
"["                      {INC_POS; return CROCHET_OUVRANT;}
"]"                      {INC_POS; return CROCHET_FERMANT;}
"{"                      {INC_POS; return ACCOLLADE_OUVRANTE;}
"}"                      {INC_POS; return ACCOLLADE_FERMANTE;}



[a-zA-Z][a-zA-Z0-9_]*   {INC_POS; 
                         // On verifie que le nom de la variable n'est pas trop long
                         if(strlen(yytext) > LONGUEUR_LEXEME_MAX){
                             fprintf(stderr, "Warning, le nom de variable '%s' en ligne %d est trop long (> %d).\n",
                                             yytext, num_ligne, LONGUEUR_LEXEME_MAX);
                             fprintf(stderr, "Il va etre tronque lors de l'insertion dans la table\n");
                             *(yytext+LONGUEUR_LEXEME_MAX+1) = '\0';
                         }
                         //insertion eventuelle et recuperation du numero lexicographique
                         yylval.entier = inserer_lexico(yytext);
                         return IDF;
                        }

" "*                    {INC_POS;}

 /* -- Les commentaires --*/
"//".*                          ;

 /* -- un caractere inconu -- */
.                      {INC_POS;} // rajouter un warning, attention comportement etrange lorsque yytext est vide ->ligne vide.

 /* Pour reconaitre un saut de ligne
 (l'incrementation de la ligne courante est faites lors de la sauvegarde de
                  la ligne courante)
 
 "\n"  ;
 */
 
%%
/* -- Routine appele par lex lorsque l'on rencontre une fin de fichier -- */
int yywrap(){
   // 0 pour continuer, mais il faut reouvrir le fichier et changer la valeur de yyin
   // 1 indique la fin du fichier
   return 1;

   // jamais execute
   // pour enlever le warnig a la compilation
    if(1 == 0) {
        input();
        yyunput(1,NULL);
        }
 }

