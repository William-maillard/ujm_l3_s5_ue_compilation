/*********************************************************************************
 *                                                                               *
 * Fonction permettant de sauvegarder un arbre frere-fils dans un fichier sous   *
 * la forme suivante :                                                           *
 * racine fils frere(fils et frere etant sauvegarde   recursivement              *
 *                   , arbre vide assurant l'arret)                              *
 *                                                                               *
 * Fonction permettant de sauvegarder les arbre et les tables d'un programme     *
 * cpyrr.                                                                        *
 *                                                                               *
 *********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "../../inc/arbres.h"
#include "../../inc/y.tab.h"
#include "../../inc/tables.h"

extern int nb_regions;

/****
 * Sauvegarde recursivement l'arbre a
 * dans le flux passe en parametre
 ****/
static void sauvegarde_arbre(arbre a, FILE *fichier) {

    // cas d'arret de la recursivite
    if(a == arbre_vide()){
        fprintf(fichier, "N ");
    }
    else{
        // sauvegarde du noeud courant
        fprintf(fichier, "%d %d ", a->nature, a->valeur);

        // sauvegarde recursif du fils
        fprintf(fichier, "S ");
        sauvegarde_arbre(a->fils, fichier);

        // sauvegarde recursif du frere
        fprintf(fichier, "B ");
        sauvegarde_arbre(a->frere, fichier);
    }
}


/****
 * Sauvegarde les arbres des region 
 * et les tables d'un programme
 * CPYRR necessaires pour executer, 
 * ulterrieurement, un programme
 ****/
void sauvegarde_programme(FILE * fichier) {
    int i;
    int n = nb_regions;

    // Arbres des regions :
    for(i=0; i<=n; i++) { 
        fprintf(fichier, "%d ", i);
        sauvegarde_arbre(table_regions[i].arbre, fichier);
    }
    fprintf(fichier, ";");


    // Table lexicographique :
    for(i=0; i<LONGUEUR_TAB_LEXICO; i++){
        if( table_lexicographique.enregistrement[i].longueur != -1) {
            
            fprintf(fichier, " %d %d %s %d",
                   i,
                   table_lexicographique.enregistrement[i].longueur,
                   table_lexicographique.enregistrement[i].lexeme,
                   table_lexicographique.enregistrement[i].suivant
                );
        }
    }
    fprintf(fichier, ";");
    
    
    // Table des declarations :
    n = table_declarations.debordement;
    for(i=0; i<n; i++) {
        if(table_declarations.t[i].nature != -1) {
            fprintf(fichier,
                    " %d %d %d %d %d %d",
                    i,
                    table_declarations.t[i].nature,
                    table_declarations.t[i].suivant,
                    table_declarations.t[i].region,
                    table_declarations.t[i].description,
                    table_declarations.t[i].execution
                );
        }
    }
    fprintf(fichier, ";");
    fprintf(fichier, " %d ;", table_declarations.debordement);

    // Table des representations
    n = table_representation.indice_libre;
    for(i=0; i<n; i++) {
        fprintf(fichier, " %d",
                table_representation.t[i]
            );
    }
    fprintf(fichier, ";");
    
    // Table des regions :
    n = nb_regions;
    for(i=0; i<=n; i++) {
        fprintf(fichier, " %d %d",
                table_regions[i].taille,
                table_regions[i].nis
            );
    }
     fprintf(fichier, ";");
}