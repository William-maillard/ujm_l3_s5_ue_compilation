#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/analyse_semantique.h"
#include "../../../inc/arbres.h"
#include "../../../inc/erreur.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"

/****
 * Verifie la semantique d'une variable :
 * - si elle est declare
 * - si les champs eventuels existe
 * - si on donne des dimensions qu'a un type tableau
 * retourne le num de declaration du type de la variable
 * Entree : pointeur sur un noeud A_VARIABLE
 * Sortie : le num_dec du type de la variable si ok,
 *          CODE_ERREUR sinon.
 ****/
int semantique_variable(arbre a_variable){
    int num ;// valeur retourne -> num_dec du type de la variable
    int id_rep; // indice dans la table des representation
    int nb_max; // nb de dimensions ou de champs d'un type construit
    int erreur = 0;
    int i, trouve;
    arbre a, a_precedent, a_liste_dim;

    // On traite le 1° IDF
    a = a_variable->fils; // jamais null, dans le yacc on donne tjrs un fils
    num = association_nom(a->valeur, TYPE_VARIABLE); // num dec de l'IDF
    if(num == -1){
        //erreur var non declaree
        affiche_ligne_erreur();
        fprintf(stderr, "La variable %s n'a pas ete declare.\n", lexeme(a->valeur));
        return CODE_ERREUR;
    }
    else{
        /* on sauvegarde son num de declaration 
           dans le noeud A_VARIABLE */
        a_variable->valeur = num;
        /* le num_dec du type de la variable */
        num = table_declarations.t[num].description;
    }
    
    while(a->frere != arbre_vide()  &&  !erreur){

        if(table_declarations.t[num].nature % TYPE_STRUCTURE != 0){ // pas une struct ni un tableau 
            // erreur le noeud d'avant etait de type simple
            // et non de type construit
            affiche_ligne_erreur();
            fprintf(stderr, "%s est de type simple et donc ne possede pas de ",
                    lexeme(a->valeur));
            if(a->frere->nature == A_IDF)
                fprintf(stderr, "champs %s\n", lexeme(a->frere->valeur));
            else
                fprintf(stderr, "dimensions\n");
            erreur++;
            
        }else{
            a_precedent = a; // pour l'affichage des erreurs
            a= a->frere;
            id_rep = table_declarations.t[num].description;
            
            if(a->nature == A_IDF){

                // On verfie que le champ courant est une structure
                if(table_declarations.t[num].nature != TYPE_STRUCTURE) {
                    affiche_ligne_erreur();
                    fprintf(stderr, "%s est un tableau et non une structure.\n",
                            lexeme(a_precedent->valeur));
                    erreur++;
                }
                else{
                    // on recherche le type du champs
                    nb_max = table_representation.t[id_rep];
                    i=0;
                    trouve=0;
                    while(i < nb_max  &&  !trouve){
                        // on regarde si le champs i est le champs du noeud
                        // en comparant les num_lexico
                        if(table_representation.t[id_rep+1 + i * 3] == a->valeur){
                            trouve = 1;
                            // on stocke le num dec de son type
                            num = table_representation.t[id_rep+2 + i * 3];
                        }
                        i++;
                    }

                    if(!trouve){
                        // erreur pas de champs correspondant
                        affiche_ligne_erreur();
                        fprintf(stderr, "%s ne contient pas de champs %s.\n",
                                lexeme(a_precedent->valeur), lexeme(a->valeur)
                            );
                        erreur++;
                    }
                }
            
            }
            // sinon liste de de dimensions
            else{
                // On verfie que le champ courant est un tableau
                if(table_declarations.t[num].nature != TYPE_TABLEAU) {
                    affiche_ligne_erreur();
                    fprintf(stderr, "%s est une structure et non un tableau.\n",
                            lexeme(a_precedent->valeur));
                    erreur++;
                }
                else{
                    // recup numero declaration du type des elems du tab
                    num = table_representation.t[id_rep+1];

                    /* on verifie qu'il y ait le bon nombre de dimensions
                       et que chaque dimension correspond a un entier */
                    nb_max = table_representation.t[id_rep];
                    a_liste_dim = a->fils;
                    i=0;
                    while(a_liste_dim != arbre_vide()  &&  !erreur){

                        // verification semantique :
                        if(semantique_valeur(a_liste_dim) != NUM_DEC_ENTIER) {
                            if(erreur == 0) { // pour l'afficher que la 1 fois
                                affiche_ligne_erreur();
                            }
                            fprintf(stderr, "La %d°dimension est incorrecte pour le tableau %s\n",
                                    i+1, lexeme(a_precedent->valeur));
                            erreur++;
                        }

                        // dimension suivante :
                        i++;
                        a_liste_dim = a_liste_dim->frere;
                    }
                    if(i != nb_max) {
                        if(erreur == 0) { affiche_ligne_erreur(); }
                        fprintf(stderr, "Le tableau %s a %d dimensions, ",
                                lexeme(a_precedent->valeur), nb_max);
                        erreur++;
                        if(i < nb_max) {fprintf(stderr, "vous n'en avez pas donne assez.\n");}
                        else { fprintf(stderr, "vous en avez trop donnee.\n");}
                    }
                }
            } // fin else liste dimension
        }
    }

    if(erreur)
        return CODE_ERREUR;
    
    return num;
}
