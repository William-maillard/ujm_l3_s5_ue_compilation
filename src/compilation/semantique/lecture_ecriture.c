#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/analyse_semantique.h"
#include "../../../inc/arbres.h"
#include "../../../inc/erreur.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"


/****
 * Verifie la correspondance entre les formats d'une chaine formatee
 * et la liste de valeurs correspondante.
 * Entree : l'arbre lire ou ecrire
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_chaine_formatee(arbre a){
    char *it = lexeme(a->fils->valeur);     /* on recup un pointeur sur la chaine      */
    arbre a_valeurs = a->fils->frere->fils; /* pointeur sur la 1 valeur de la liste
                                               des valeurs correspondantes aux formats */
    int type_valeur, type_format;
    int erreur = 0, nb_formats = 0;
    
    // On parcours la chaine a la recherche de format :
    while(*it != '\0') {

        // caractere echapee
        if(*it == '\\'){
            it+=2;//tjrs suivie d'un char, on le saute.
        }  
        else if(*it == '%'){
            it++;
            // le % indique -t- il un format ?
            nb_formats++;
            switch(*it){
            case 'd' : type_format= NUM_DEC_ENTIER; break;
            case 'f' : type_format= NUM_DEC_REEL; break;
            case 'c' : type_format= NUM_DEC_CARACTERE; break;
            case 's' : type_format= NUM_DEC_CHAINE; break;
            //case 'b' : type_format= NUM_DEC_BOOLEEN; break;
            case '%' : nb_formats--; break;
            default :
                if(erreur == 0) { affiche_ligne_erreur(); }
                fprintf(stderr, "Le %d format est inconu : %%%c\n", nb_formats, *it);
                //il ne faut pas continuer le else if
                erreur++;
                type_format = CODE_ERREUR;
            }
            if(type_format != CODE_ERREUR) {
                // a -t- on une expression correspondante ?
                if(a_valeurs == arbre_vide()){
                    if(erreur == 0) { affiche_ligne_erreur(); }
                    fprintf(stderr, "Le %d format %%%c attend une valeur mais vous n'en avez pas donnee.\n", nb_formats,  *it);
                    erreur++;
                }
                else{
                    /* On recupere le type de la valeur et
                       la compare au type du format */
                    type_valeur= semantique_valeur(a_valeurs);
                    if(type_valeur != type_format){
                        if(erreur == 0) { affiche_ligne_erreur(); }
                        fprintf(stderr, "Le %d format attend une valeur de type %s. Vous avez donnez une valeur de type %s\n",
                                nb_formats, type_simple_chaine(type_format),
                                type_simple_chaine(type_valeur));
                        erreur++;
                    }
                
                    // valeur suivante
                    a_valeurs = a_valeurs->frere;
                }
                type_format = -1;
            }
        }
        // on cherche un autre format
        it++;
    }
    
    if(a_valeurs != arbre_vide()){
        if(erreur == 0) {affiche_ligne_erreur();}
        printf("La chaine contient %d format, vous en avez donne plus.\n", nb_formats);
        return CODE_ERREUR;
    }
    if(erreur){
        return CODE_ERREUR;
    }   
    
    return CODE_VALIDE;
}