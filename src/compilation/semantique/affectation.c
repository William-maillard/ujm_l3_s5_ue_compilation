#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/analyse_semantique.h"
#include "../../../inc/arbres.h"
#include "../../../inc/erreur.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"

/****
 * Pour les messages d'erreur, permet d'afficher
 * sous forme de chaine de caractere les types
 * simples du langage CPYRR a partir de leur
 * macro donnee en parametre
 ****/
char *type_simple_chaine(int n) {
    switch(n){
    case NUM_DEC_ENTIER: return "int";
    case NUM_DEC_REEL: return "float";
    case NUM_DEC_CARACTERE: return "char";
    case NUM_DEC_BOOLEEN: return "bool";
    default:
        return "compose";
    }
}

/****
 * Verifie la semantique d'une valeur.
 * Appel les fonctions de semantiques concerne suivant 
 * la valeur
 * Entree : un arbre d'une valeur
 * Sortie : num dec du type de la valeur, CODE_ERREUR sinon
 ****/
int semantique_valeur(arbre a) {

    switch(a->nature){
        /* ----- LES CONSTANTES ----- */
        
    case A_CSTE_ENTIERE:
        return NUM_DEC_ENTIER;
    case A_CSTE_REEL:
        return NUM_DEC_REEL;
    case A_CSTE_CARACTERE :
        return NUM_DEC_CARACTERE;
    case A_CSTE_CHAINE:
        return NUM_DEC_CHAINE;
    case A_CSTE_BOOLEEN:
        return NUM_DEC_BOOLEEN;

        
        /* ----- VARIABLES ET APPELS ----- */
        
    case A_VARIABLE:
        return semantique_variable(a);
    case A_APPEL_PROC:
    case A_APPEL_FCT:
        return semantique_appel(a);

        
        /* ----- EXPRESSIONS ----- */
        
    case A_EXPRESSION_ARITHMETIQUE:
        // valeur noeud = le type des operandes
        a->valeur = semantique_expression_arithmetique(a->fils);
        return a->valeur;
    case A_EXPRESSION_RELATIONNELLE :
        // valeur noeud = le type des operandes
        a->valeur = semantique_expression_relationnelle(a->fils);
        // renvoie le type de l'expression
        return (a->valeur == CODE_ERREUR)? CODE_ERREUR:NUM_DEC_BOOLEEN;
    case A_EXPRESSION_BOOLEENNE:
        return semantique_expression_booleenne(a->fils);

        /* ----- AU CAS OU ... ----- */        
    default:
        fprintf(stderr, "Erreur (semantique valeur), valeur inconnu ligne %d!!\n", num_ligne);
        affiche_arbre(a);
        return CODE_ERREUR;
    }
}



/****
 * Verifie que la partie droite est une variable valide,
 * et que la partie gauche est une valeur valide de
 * meme type.
 * Entree : un pointeur sur un noeud A_AFFECTATION
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_affectation(arbre a) {
    int type_var, type_valeur;


    
    type_var = semantique_variable(a->fils);
    if(type_var == CODE_ERREUR){
        // message d'erreur deja genere par la fct precedente
        return CODE_ERREUR;
    }

    type_valeur = semantique_valeur(a->fils->frere);
    if(type_valeur == CODE_ERREUR)
        // message d'erreur genere par la fct precedente
        return CODE_ERREUR;

    if(type_var == type_valeur){
        return CODE_VALIDE;
    }

    // sinon erreur :
    affiche_ligne_erreur();
    fprintf(stderr, "La partie gauche de l'affectation est de type %s.\n",
            type_simple_chaine(type_var));
    fprintf(stderr, "La partie droite de l'affectation est de type %s.\n",
            type_simple_chaine(type_valeur));
    fprintf(stderr, "Cela est interdit en CPYRR.\n");
    return CODE_ERREUR;
}

