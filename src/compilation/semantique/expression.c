#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/analyse_semantique.h"
#include "../../../inc/arbres.h"
#include "../../../inc/erreur.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"


/****
 * Verifie la semantique d'une expression arithmetique,
 * Entree : l'abre d'une operation arithmetique
 * Sortie : le num dec du type du resultat de cette expression
 * (entier ou reel), CODE_ERREUR sinon
 ****/
int semantique_expression_arithmetique(arbre a) {
    int operande[2];

    switch(a->nature){
        /* ------ OPERATIONS -> RECURSIVITE ------ */
    case A_PLUS:
    case A_MOINS:
    case A_MULT:
    case A_DIV:
        operande[0] = semantique_expression_arithmetique(a->fils);
        if(operande[0] == CODE_ERREUR) {
            return CODE_ERREUR;
        }
        if(operande[0] != NUM_DEC_ENTIER  &&  operande[0] != NUM_DEC_REEL) {
            fprintf(stderr, "0 Erreur expressions arithmetique ligne %d : une operande doit etre de type entier ou reel.\n", num_ligne);
            return CODE_ERREUR;
        }
        
        operande[1] = semantique_expression_arithmetique(a->fils->frere);
        if(operande[1] == CODE_ERREUR){
            return CODE_ERREUR;
        }
        if(operande[1] != NUM_DEC_ENTIER  &&  operande[1] != NUM_DEC_REEL) {
            fprintf(stderr, "1 Erreur expressions arithmetique ligne %d : une operande doit etre de type entier ou reel.\n", num_ligne);
            return CODE_ERREUR;
        }      
        break;
        
    case A_MOD:
        operande[0] = semantique_expression_arithmetique(a->fils);
         if(operande[0] == CODE_ERREUR) {
            return CODE_ERREUR;
        }
        if(operande[0] != NUM_DEC_ENTIER) {
            fprintf(stderr, "Erreur expression arithmetique ligne %d : les operandes d'un modulo doivent etre de type entier.\n", num_ligne);
            return CODE_ERREUR;
        }
        operande[1] = semantique_expression_arithmetique(a->fils->frere);
        if(operande[1] == CODE_ERREUR){
            return CODE_ERREUR;
        }
        if(operande[1] != NUM_DEC_ENTIER) {
            fprintf(stderr, "Erreur expression arithmetique ligne %d : les operandes d'un modulo doivent etre de type entier.\n", num_ligne);
            return CODE_ERREUR;
        }
        break;
        
        /* ----- UNE VALEUR -> CAS D'ARRET RECURSIVITE----- */
    default:
        return semantique_valeur(a);
    }

    // Pour les operations : les operandes sont-elles de meme type ?
    if(operande[0] == operande[1]){
        return operande[0];
    }
    affiche_ligne_erreur();
    fprintf(stderr, "Erreur expression arithmetique ligne %d : les operandes doivent etre de meme type.\n", num_ligne);
    return CODE_ERREUR;
}



/****
 * Verifie la semantique d'un expression relationnelle,
 * les operandes doivente etre de meme type (entier ou reel)
 * Entree : l'arbre d'une operation relationnelle
 * Sortie : NUM_DEC_{ENTIER, REEL} si ok, CODE_ERREUR sinon
 ****/
int semantique_expression_relationnelle(arbre a) {
    int operande[2];

    operande[0] = semantique_expression_arithmetique(a->fils);
    operande[1] = semantique_expression_arithmetique(a->fils->frere);
    
    if(operande[0] == operande[1])
        return operande[0];

    affiche_ligne_erreur();
    fprintf(stderr, "Erreur expression relationnelle: les operandes doivent etre soit entier soit reel et de meme type.\n");
    return CODE_ERREUR;
}


/****
 * Verifie la semantique d'une expression boolenne,
 * les operande doivent etre des booleens.
 * Entree : l'arbre d'une operation booleenne
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon 
****/
int semantique_expression_booleenne(arbre a) {
    int operande[2];
    
    switch(a->nature) {
        /* ----- OPERATEUR BOOLEEN -> RECURSIF ----- */
    case A_OU:
    case A_ET:
        operande[0] = semantique_expression_booleenne(a->fils);
        if(operande[0] != CODE_ERREUR) {
            operande[1] = semantique_expression_booleenne(a->fils->frere);
            if(operande[1] == CODE_ERREUR) {
                printf("Erreur expression booleenne : les operande doivent etre de type booleen.\n");
                return CODE_ERREUR;
            }
        }
        else{
            printf("Erreur expression booleenne : les operande doivent etre de type booleen.\n");
            return CODE_ERREUR;
        }
        break;
    case A_NON:
        operande[0] = semantique_expression_booleenne(a->fils);
        break;
        
        /* ----- UNE VALEUR -> ARRET RECURSIVITE ----- */
    default:
        operande[0] = semantique_valeur(a);
    }

    if(operande[0] != NUM_DEC_BOOLEEN) {
        affiche_ligne_erreur();
        fprintf(stderr, "Erreur, la valeur doit etre de type booleen.\n");
        return CODE_ERREUR;
    }

    //les deux operandes son correcte et de type booleen
    return NUM_DEC_BOOLEEN;
}