// NB : fonction plus utilisee.
#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/analyse_semantique.h"
#include "../../../inc/arbres.h"
#include "../../../inc/erreur.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"

/****
 * Verifie la semantique d'une instruction 
 * (et donc recursivement de tous les elements
 *  qui la composent)
 * Entree : noeud liste instruction
 * Sortie : CODE_VALIDE si ok sinon CODE_ERREUR
 ****/
int semantique_instruction(arbre a) {

    switch(a->nature) {
    case A_AFFECTATION:
        return semantique_affectation(a);
    case A_TANT_QUE:
    case A_INST_COND:
        return semantique_valeur(a->fils);
    case A_RETOURNE:
        return semantique_retourne(a->fils);
    case A_APPEL_FCT:
    case A_APPEL_PROC:
        return semantique_appel(a);
    case A_LIRE:
    case A_ECRIRE:
        return semantique_chaine_formatee(a);
    case A_LISTE_INSTRUCTION:
        return CODE_VALIDE; // instruction vide ';'
    default:
        fprintf(stderr, "Erreur ligne %d : instruction inconue !!\n", num_ligne);
        return CODE_ERREUR;
    }
}