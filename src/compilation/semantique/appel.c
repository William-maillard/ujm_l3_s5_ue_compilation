#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/analyse_semantique.h"
#include "../../../inc/arbres.h"
#include "../../../inc/erreur.h"
#include "../../../inc/pile_region.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"

/**** 
 * Verifie la semantique d'un appel :
 *   - nombre de prametres
 *   - type de chaque parametre 
 * Entree : l'arbre d'un appel
 * Sortie : le type de retour si ok, CODE_ERREUR sinon
 ****/
int semantique_appel(arbre a) {
    arbre a_param;
    int nb_param, indice_type_parametre;
    int num_dec_param;
    char *type; // pour les affichage d'erreur;
    int cpt=0;//nombre de parametres traite
    int erreur=0;

    // Recup du nombre de parametres a partir du num_decl de proc/fct
    nb_param = table_representation.t[table_declarations.t[a->valeur].description];

    /*
      Pour recuperer l'indice du num de declaration de l'eventuel 1er parametre il y a 2 cas :
        -Fonction : decallage de 2 +1 (car on stocke le nb_param et le type retour)
        -Procedure : decallage de 1 +1
        (+1 pour avoir le num de declaration)
     */
    if(a->nature == A_APPEL_PROC){
        indice_type_parametre = table_declarations.t[a->valeur].description + 2;
        type = "procedure";
    }
    else{
        indice_type_parametre = table_declarations.t[a->valeur].description + 3;
        type = "fonction";
    }

    //Pointeur sur le 1er parametre
    a_param = a->fils->fils;
    
    while(a_param != arbre_vide()) {

        /* a -t-on depasse le nombre de parametres requis ? */
        if(++cpt > nb_param) {
            erreur++;
            affiche_ligne_erreur();
            fprintf(stderr,"La %s attend %d parametre, vous en avez donnez plus.\n",
                    type, nb_param);
            break;
        }

        num_dec_param= semantique_valeur(a_param);

        if(num_dec_param == CODE_ERREUR) {
            // la ligne d'erreur est afficher pas semantique_valeur()
            printf("Parametre %d incorecte.\n", cpt);
            erreur++;
        }
        else if(num_dec_param != table_representation.t[indice_type_parametre]) {
            if(erreur == 0) { affiche_ligne_erreur(); }
            fprintf(stderr,"Le %d parametre doit etre de type %s, il est de type %s.\n",
                    cpt, type_simple_chaine(table_representation.t[indice_type_parametre]),
                    type_simple_chaine(num_dec_param)
                    );
                erreur++;
        }

        // param suivant 
        a_param = a_param->frere;
        indice_type_parametre +=3;
        
    }// fin semantique des types des parametres

    
    if(cpt != nb_param){
        if(erreur == 0) { affiche_ligne_erreur(); }
        fprintf(stderr,"Nombre de parametres insuffisant, il en faut %d.\n", nb_param);
        return CODE_ERREUR;
    }
    
    if(erreur) {
        return CODE_ERREUR;
    }
    
    if(a->nature == A_APPEL_FCT)
        // retourne le num_dec du type de retour pour une fonction
        return table_representation.t[table_declarations.t[a->valeur].description+1];
    // sinon procedure
    return NUM_DEC_VIDE;
}



/****
 * Verifie que la valeur retourne correspond
 * au type de retour de la fonction courante
 * Entree : pointeur sur le noeud de la valeur retournee
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_retourne(arbre a) {
    int type_retourne;

    /* La valeur du noeud A_RETOURNE est 
       le numero de declaration du type retourne
     */
    if(type_retour() == NUM_DEC_VIDE  &&  a != arbre_vide()) {
        affiche_ligne_erreur();
        fprintf(stderr, "La region courante ne retourne rien.\n");
        return CODE_ERREUR;
    }

    // on retourne le type de l'expression retourne
    if(a == arbre_vide())
        type_retourne = NUM_DEC_VIDE;
    else
        type_retourne = semantique_valeur(a);

    if(type_retour() == type_retourne)
        return CODE_VALIDE;

    affiche_ligne_erreur();
    fprintf(stderr, "La fonction retourne un %s, la valeur retournee est de type %s.\n",
            type_simple_chaine(type_retour()), type_simple_chaine(type_retourne));
    return CODE_ERREUR;
}



/****
 * Verifie qu'une fonction qui retourne une valeur
 * se termine par un return.
 * Entree : arbre d'un block d'instruction
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_corps_fonction(arbre a){

    if(type_retour() == NUM_DEC_VIDE)
        return CODE_VALIDE;

    // on parcours l'arbre jusqu'a la derniere instruction :
    while(a->fils->frere != arbre_vide()) {
        a= a->fils->frere; // instruction suivante
    }

    if(a->fils == arbre_vide()  ||  a->fils->nature != A_RETOURNE) {
        printf("Erreur semantique (l %d) : Le corps de la fonction doit se terminer par une instruction retourne.\n", num_ligne);
        return CODE_ERREUR;
    }
    else
        return CODE_VALIDE;
}