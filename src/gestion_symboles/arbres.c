/*************************************
 *                                   *
 * Fonctions pour creer              *
 * et manipuler des arbres binaires  *
 * Pere : Fils - Frere               *
 *                                   *
 *************************************/
#include <stdio.h>
#include <stdlib.h>
#include "../../inc/arbres.h"
#include "../../inc/tables.h"

/****
 * Cree un arbre vide 
 **** */
arbre arbre_vide() { return NULL; }

/* *****
 * Créer un noeud et renvoie le pointeur sur ce noeud. 
 ***** */
arbre creer_noeud(int nature, int valeur){
    arbre new_node = NULL;

    new_node = (arbre) malloc(sizeof(noeud));
    
    if(new_node == NULL)
    {
        printf("pbm malloc(). Fin du programme.\n");
        exit(-1);
    }
        
    new_node->nature = nature;
    new_node->valeur = valeur;
    new_node->frere = NULL;
    new_node->fils = NULL;

    return new_node;
}

/* ***** 
 * Ajoute fils au noeud pere et renvoie un pointeur sur le père. 
 ***** */
arbre concat_pere_fils(arbre pere, arbre fils){
    pere->fils= fils;

    return pere;
   
}

/* ***** 
 * Ajoute frere au noeud pere et renvoie un pointeur sur le pere. 
 ***** */
arbre concat_pere_frere(arbre pere, arbre frere){
    pere->frere= frere;

    return pere;
}

/* ***** 
 * Supprime un arbre et libère la mémoire occupée.
 *  Renvoie le pointeur NULL
 ***** */
arbre detruire_arbre(arbre a){
    
    if(a != arbre_vide()){
        a->fils = detruire_arbre(a->fils);
        a->frere = detruire_arbre(a->frere);
        free(a);
    }
    return NULL;
}


/* *****
 * Affiche l'arbre a sur la sortie standard.
 ***** */
static void affiche_arbre_bis(arbre a, int decallage){

    fprintf(stdout,"%*c", decallage, '|');

    
    switch(a->nature) {
    case A_PROG : fprintf(stdout,"A_PROG");
        break;
    case A_LISTE_INSTRUCTION : fprintf(stdout,"A_LISTE_INSTRUCTION");
        break;
    case A_RETOURNE : fprintf(stdout,"A_RETOURNE");
        break;
    case A_INST_COND : fprintf(stdout,"A_INST_COND");
        break;
    case A_TANT_QUE : fprintf(stdout,"A_TANT_QUE");
        break;
    case A_AFFECTATION : fprintf(stdout,"A_AFFECTATION");
        break;
    case A_LISTE_INDICES : fprintf(stdout,"A_LISTE_INDICES");
        break;
    case A_IDF : fprintf(stdout,"A_IDF");
        break;
    case A_VARIABLE : fprintf(stdout,"A_VARIABLE");
        break;
    case A_CSTE_ENTIERE : fprintf(stdout,"A_CSTE_ENTIERE");
        break;
    case A_CSTE_REEL : fprintf(stdout,"A_CSTE_REEL");
        break;
    case A_CSTE_BOOLEEN : fprintf(stdout,"A_CSTE_BOOLEEN");
        break;
    case A_CSTE_CARACTERE : fprintf(stdout,"A_CSTE_CARACTERE");
        break;
    case A_CSTE_CHAINE : fprintf(stdout,"A_CSTE_CHAINE");
        break;
    case A_EXPRESSION_ARITHMETIQUE :
        fprintf(stdout,"A_EXPRESSION_ARITHMETIQUE");
        break;
    case A_EXPRESSION_BOOLEENNE :
        fprintf(stdout,"A_EXPRESSION_BOOLEENNE");
        break;
    case A_EXPRESSION_RELATIONNELLE :
        fprintf(stdout,"A_EXPRESSION_RELATIONNELLE");
        break;
    case A_PLUS : fprintf(stdout, "A_PLUS");
        break;
    case A_MOINS : fprintf(stdout,"A_MOINS");
        break;
    case A_MULT : fprintf(stdout,"A_MULT");
        break;
    case A_DIV : fprintf(stdout,"A_DIV");
        break;
    case A_MOD : fprintf(stdout,"A_MOD");
        break;
    case A_OU : fprintf(stdout,"A_OU");
        break;
    case A_ET : fprintf(stdout,"A_ET");
        break;
    case A_NON : fprintf(stdout,"A_NON");
        break;
    case A_INF : fprintf(stdout,"A_INF");
        break;
    case A_INF_EGAL : fprintf(stdout,"A_INF_EGAL");
        break;
    case A_SUP : fprintf(stdout,"A_SUP");
        break;
    case A_SUP_EGAL : fprintf(stdout,"A_SUP_EGAL");
        break;
    case A_EGAL : fprintf(stdout,"A_EGAL");
        break;
    case A_DIFFERENT : fprintf(stdout,"A_DIFFERENT");
        break;
    case A_APPEL_FCT : fprintf(stdout,"A_APPEL_FCT");
        break;
    case A_APPEL_PROC : fprintf(stdout,"A_APPEL_PROC");
        break;
    case A_LISTE_PARAM : fprintf(stdout,"A_LISTE_PARAM");
        break;
    case A_LIRE :
        fprintf(stdout,"A_LIRE");
        break;
    case A_ECRIRE :
        fprintf(stdout,"A_ECRIRE");
        break;
    case A_LISTE_VARIABLES:
         fprintf(stdout,"A_LISTE_VARIABLE");
        break;
    }

    if(a->nature == A_IDF || a->nature == A_CSTE_REEL || a->nature == A_CSTE_CHAINE)
        fprintf(stdout," : %s\n", lexeme(a->valeur));
    else
        fprintf(stdout," : %d\n",a->valeur);


    if(a->fils != NULL)
        affiche_arbre_bis(a->fils, decallage+6);
    else{
        // NB : il y a un decallage si on fait en un printf
        fprintf(stdout,"%*c", decallage+6, '|');
        fprintf(stdout,"fils NULL\n");
        }
        
    if(a->frere != NULL)
        affiche_arbre_bis(a->frere, decallage);
    else{
        fprintf(stdout,"%*c", decallage, '|');
        fprintf(stdout,"frere NULL\n");
        }
}

void affiche_arbre(arbre a){
    affiche_arbre_bis(a, 0);
}