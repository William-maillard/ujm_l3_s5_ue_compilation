/*******************************************************************
 *                                                                 *
 * Definition des fonctions concernant la table des declarations : *
 *                                                                 *
 *     - Initialisation                                            *
 *     - Insertion                                                 *
 *     - Association nom                                           *
 *     - Affichage                                                 *
 *                                                                 *
 *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../../inc/pile_region.h"
#include "../../../inc/tables.h"

// globals variables
structure_dec table_declarations;

/****
 * Initialise la table des declarations :
 *   - les 4 premieres lignes avec les types simple
 *   - les autres a -1
 *** */
void init_table_declarations()
{
    int i;

    for (i = 0; i < 4; i++)
    {
        table_declarations.t[i] = (infos_declaration){TYPE_BASE, -1, 0, 0, 1};
    }
    memset(&(table_declarations.t[4]), -1, (LONGUEUR_TAB_DECLARATION - 4) * sizeof(infos_declaration));
    table_declarations.debordement = LONGUEUR_TAB_LEXICO;
}

/* ***** INSERTION DANS LES TABLES :  DECLARATION  ***** */

/* ****
 * insertion dans la table des déclarations
 * retour : - l'indice dans la table (>0)
 *          - -1 en cas de redeclaration d'une var de meme type
 * (et reserve la place dans la table des representation)
 ***** */
int inserer_dec(nature nat, int num_lexico)
{
    int precedent, identique = 0;
    int num_dec = num_lexico;

    // a -t-on deja declaree une variable de meme nom ?
    if (table_declarations.t[num_dec].nature != -1)
    {
        /*
         * on parcours le chainage pour s'assurer qu'on ne
         * redeclare pas la meme variable
         */

        do
        {
            precedent = num_dec;
            if (table_declarations.t[precedent].nature % nat == 0 &&
                table_declarations.t[precedent].region == region_courante())
            {
                identique = 1;
            }
            else
            {
                num_dec = table_declarations.t[precedent].suivant;
            }
        } while (num_dec != -1 && !identique);

        if (identique != 0)
        {
            fprintf(stderr, "Redeclaration de  %s, changer la ligne ci-dessous :\n", table_lexicographique.enregistrement[num_lexico].lexeme);
            return -1;
        }

        // l'indice de la dec a inserer se trouve dans la partie debordement
        num_dec = table_declarations.debordement++;
        // on l'insere dans le chainage
        table_declarations.t[precedent].suivant = num_dec;
    }

    /*
     * On insere la declaration dans la table :
     */
    // 1. Colonnes nature et region
    table_declarations.t[num_dec].nature = nat;

    if (nat == TYPE_FONCTION || nat == TYPE_PROCEDURE)
    {
        /* on a changer de region en recontrant le mot cle function|procedure
           il faut prendre la region d'avant */
        table_declarations.t[num_dec].region = num_region_englobante(1);
    }
    else
    {
        table_declarations.t[num_dec].region = region_courante();
    }

    // 2. Colonne description  -> depend de la nature de la declaration
    switch (nat)
    {
    case TYPE_VARIABLE:
        // contient le num de declaration du type de la variable
        // conf fonction ajouter_description_variable(int num_dec, int num_dec_type)
        // du coups ici ne rien faire
        break;

    case TYPE_STRUCTURE:
    case TYPE_TABLEAU:
    case TYPE_FONCTION:
    case TYPE_PROCEDURE:
        /*
         * On reserve l'indice libre dans la table des representation des types et
         * et des en-tete de sous programme.
         * La case de cet indice va contenir le nombre de champs pour une structure,
         * le nombre de dimensions pour un tableau et le nombre de parametres pour une
         * fonction.
         * Cette case va etre incremente a chaque insertion de champs/dimension/parametre
         */
        table_declarations.t[num_dec].description = table_representation.indice_libre;
        table_representation.t[table_representation.indice_libre] = 0;
        table_representation.indice_libre++;
        if (nat == TYPE_FONCTION || nat == TYPE_TABLEAU)
        {
            /* 2° case :
             *   - fonction = le type de retour
             *   - tableau = le type des elems du tableau
             */
            table_representation.indice_libre++;
        }
        break;

    default: // pour faire plaisir a gcc
        break;
    }

    // 3. Colonne execution  -> depend de la nature de la declaration
    switch (nat)
    {
    case TYPE_VARIABLE:
        /*
         * contient le deplacement dans la pile (par rapport a la BC de la region courante)
         * NB la taille de la region sera actualiser dans la fonction
         * completer_dec_variable
         */
        table_declarations.t[num_dec].execution = table_regions[region_courante()].taille;
        break;
    case TYPE_STRUCTURE:
    case TYPE_TABLEAU:
        /*
         * contient la taille de la structure|tableau
         * sera increment a chaque ajout de champs pour les struct
         * (cf fct pour la table de representation)
         * apres l'insertion de toutes les dimensions pour un tableau
         */
        table_declarations.t[num_dec].execution = 0;
        break;
    case TYPE_FONCTION:
    case TYPE_PROCEDURE:
        /*
         * Contient le numero de la region de la fonction/procedure.
         */
        table_declarations.t[num_dec].execution = region_courante();
        break;
    default: // pour faire plaisir a gcc
        break;
    }

    return num_dec;
}

/* ***
 * Remplis la colonne descriptions pour une variable
 * et ajoute l'espace memoire occupe par la variable a la region courante
 *** */
void completer_dec_variable(int num_dec, int num_dec_type)
{

    // colonne description = indice table declaration du type
    table_declarations.t[num_dec].description = num_dec_type;
    // ajout de la taille de la variable a la region
    table_regions[region_courante()].taille += table_declarations.t[num_dec_type].execution;
}

/* ==================== FIN INSERTION ==================== */

/* ***
 * Retourne le numero de declaration associe au non de variable passe en parametre.
 * En prenant en compte les variables globales/locales definies
 * -1 si pas de lexeme de nature nat accessible depuis la region courante
 *** */
int association_nom(int num_lexico, nature nat)
{
    int num_dec;
    int suivant, trouve = 0;
    int niv_global = TAILLE_MAX_PILE; /* stocke le niveau de la region englobante de la
                                         dec trouve par rapport a la region courante */
    int tmps;

    /*
       On va cherche son num de declaration dans la region courante ou une des regions
      englobantes
    */
    trouve = 0;
    suivant = num_lexico;
    do
    {
        num_dec = suivant;
        if (table_declarations.t[num_dec].nature % nat == 0)
        {
            // on a trouve une declaration de type nat
            tmps = region_englobante(table_declarations.t[num_dec].region);
            if (tmps == 0)
            {
                // on a trouve une declaration dans la region courante
                trouve = num_dec;
                suivant = -1;
            }
            else
            {
                // fait-elle partie des regions englobante de la region courrante
                // et a quel niveau d'imbrication ?
                if (tmps > -1 && tmps < niv_global)
                {
                    // on a trouve une redeclaration de la var dans la region supperieur
                    niv_global = tmps;
                    trouve = num_dec;
                }
                // si oui on cherche si elle est pas masque dans la region supp a tmps
                // si non on continue de chercher.
                suivant = table_declarations.t[num_dec].suivant;
            }
        }
        else
        {
            suivant = table_declarations.t[num_dec].suivant;
        }
    } while (suivant != -1);

    // a-t-on bien trouve une declaration ?
    if (!trouve)
    {
        return -1;
    }

    return trouve;
}

/****
 * Affiche la table des declarations
 *** */
void affiche_table_declarations()
{
    int i, n = table_declarations.debordement;
    printf("/====================TABLE DECLARATIONS==================\\\n");
    printf("|  i |     nature     | suivant | region | descrp | exec |\n");
    for (i = 0; i < n; i++)
    {
        if (table_declarations.t[i].nature != -1)
        {
            printf("|%3d |", i);
            switch (table_declarations.t[i].nature)
            {
            case TYPE_BASE:
                printf("    TYPE_BASE   |");
                break;
            case TYPE_STRUCTURE:
                printf(" TYPE_STRUCTURE |");
                break;
            case TYPE_TABLEAU:
                printf("  TYPE_TABLEAU  |");
                break;
            case TYPE_VARIABLE:
                printf("  TYPE_VARIABLE |");
                break;
            case TYPE_PROCEDURE:
                printf(" TYPE_PROCEDURE |");
                break;
            case TYPE_FONCTION:
                printf(" TYPE_FONCTION  |");
                break;
            default:
                printf("     -1         |");
            }
            printf(" %4d    | %3d    | %3d    | %3d  |\n",
                   table_declarations.t[i].suivant,
                   table_declarations.t[i].region,
                   table_declarations.t[i].description,
                   table_declarations.t[i].execution);
        }
    }

    printf("\\========================================================/\n\n");
}
