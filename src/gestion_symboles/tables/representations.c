/**********************************************************************
 *                                                                    *
 * Definition des fonctions concernant la table des representations : *
 *                                                                    *
 *     - Initialisation                                               *
 *     - Insertion                                                    *
 *         > champ d'une structure                                    *
 *         > dimension d'un tableau                                   *
 *         > taille memoire occupe par un tableau                     *
 *         > type retour d'une fonction                               *
 *         > parametre d'une fonction ou procedure                    *
 *     - Association nom                                              *
 *     - Affichage                                                    *
 *                                                                    *
 **********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/erreur.h"
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"

// globals variables
structure_rep table_representation;
int nb_erreurs;

/****
 * Initialisation de la table des representations
 *** */
void init_table_representations()
{
    table_representation.indice_libre = 0;
}

/* ***
 * Ajoute un champs a la structure dont l'indice
 * dans la table des declarations est passe en parametre.
 * Chaque champs prend 3 cases dans la table de representation :
 *   - numero lexico
 *   - indice declaration de son type
 *   -  deplacement par rapport au debut de la structure pour atteindre le champs.
 * Retourne -1 si un champs du meme nom existe deja.
 *** */
int ajouter_champs(int num_lexico_champs, int num_dec_type, int num_dec_struct)
{
    int indice_table_rep = table_declarations.t[num_dec_struct].description;
    int i, j;

    // on se place apres les champs deja declares
    i = indice_table_rep + 3 * table_representation.t[indice_table_rep] + 1;

    // on verifie qu'il n'y a pas de champs du meme nom
    for (j = indice_table_rep + 1; j < i; j += 3)
    {
        if (table_representation.t[j] == num_lexico_champs)
        {
            fprintf(stderr, "Un champs de la structure porte deja ce nom : %s\n",
                    lexeme(num_lexico_champs));
            fprintf(stderr, "Veuillez corriger la ligne suivante :\n");
            return -1; // on ajoute pas le champs.
        }
    }

    // on insere le champs
    table_representation.t[i] = num_lexico_champs;
    table_representation.t[i + 1] = num_dec_type;

    // son decallage vaut la taille actuelle de la structure
    table_representation.t[i + 2] = table_declarations.t[num_dec_struct].execution;

    // on increment le nombre de champs de la structure
    table_representation.t[indice_table_rep]++;

    // on incremente la taille de la structure (colonne execution) par la taille du champs
    table_declarations.t[num_dec_struct].execution += table_declarations.t[table_representation.t[i + 1]].execution;

    // on decalle l'indice libre de la table des representations
    table_representation.indice_libre += 3;

    return 0;
}

/* ***
 * ajoute une dimension au tableau
 * Pour l'instant on ne gere PAS les dim donnees sous forme d'expression
 *** */
void ajouter_dimension(int borne_inf, int borne_supp, int num_dec_tab)
{
    int indice_rep = table_declarations.t[num_dec_tab].description;
    int i;

    // on positionne notre indice apres les 2° cases (nb dim & type) et les eventuelles
    // dimensions deja presentent (occupe 2 cases)
    i = indice_rep + 2 + 2 * table_representation.t[indice_rep];

    // on insere la dimension
    table_representation.t[i] = borne_inf;
    table_representation.t[i + 1] = borne_supp;
    table_representation.t[indice_rep]++;

    // on decalle l'indice libre de la table des representations
    table_representation.indice_libre += 2;
}

/* ***
 * Appele a la fin de la declaration d'un tableau,
 * Permet d'ajouter le type des elements d'un tableau
 * et sa taille dans la colonne execution de la table des declarations
 *** */
void inserer_type_taille_tableau(int num_dec_tab, int num_dec_type)
{
    int num_rep;
    int taille_tableau, taille_element;
    int nb_dim, nb_elements = 1;
    int i, j;

    // recup l'indice dans la table de representation
    num_rep = table_declarations.t[num_dec_tab].description;

    // 1. ajout du type des elems du tableau
    table_representation.t[num_rep + 1] = num_dec_type;

    // 2. on recupere la taille du type
    taille_element = table_declarations.t[num_dec_type].execution;

    // 3. on calcule le nombre d'element du tableau
    nb_dim = table_representation.t[num_rep];
    for (i = 1; i <= nb_dim; i++)
    {
        j = num_rep + i * 2;
        // [a..b] -> b-a+1
        nb_elements *= table_representation.t[j + 1] - table_representation.t[j] + 1;
    }

    // 4. on en deduis la taille
    taille_tableau = nb_elements * taille_element;

    // 5. on ajoute cette taille a la colonne execution
    table_declarations.t[num_dec_tab].execution = taille_tableau;
}

/****
 * Ajoute le type de retour dans la table des representations,
 * a la fonction/procedure dont le num de representation est passe en parametre
 *** */
void inserer_type_retour(int num_rep_fct, int num_dec_type)
{
    // NB : nb_param | typ | <les parametres>
    table_representation.t[num_rep_fct + 1] = num_dec_type;
}

/****
 * Insere un parametre dans les tables :
 *   - le numero lexico de son lexeme
 *   - sa declaration dans la tab des declaration
 *   - son num_lexico|num_dec|num_dec_type dans la table des declarations
 * a la fonction/procedure dont le num de declaration est passe en parametre
 *** */
void ajouter_parametre(int num_rep_fct, int num_lexico, int num_dec_type)
{
    int num_dec;
    int i = table_representation.indice_libre;

    // insertion de la declaration du parametre
    num_dec = inserer_dec(TYPE_VARIABLE, num_lexico);
    completer_dec_variable(num_dec, num_dec_type);

    // insertion des infos du prametre
    table_representation.t[i] = num_lexico;
    table_representation.t[i + 1] = num_dec_type;
    /* son deplacement vaut la taille du chainage + le nb de param
       deja inserer (que des types simples)*/
    table_representation.t[i + 2] = TAILLE_CHAINAGE + table_representation.t[num_rep_fct];

    // on incremente les differents compteurs
    table_representation.t[num_rep_fct]++;
    table_representation.indice_libre += 3;
}

/****
 * Affiche la table des representations
 *** */
void affiche_table_representations()
{
    int i, j, n = table_representation.indice_libre;
    FILE *flux = stdout;

    printf("TABLE DES REPRESENTATIONS :\n");
    for (j = 0; j < 20; j++)
        printf("| %2d ", j);
    printf("|\n");
    fprintf(flux, "| %2d ", table_representation.t[0]);
    for (i = 1; i < n; i++)
    {
        if (i % 20 == 0)
        {
            printf("|\n\n");
            for (j = 1; j <= 20; j++)
                printf("| %2d ", i + j);
            printf("|\n");
        }
        fprintf(flux, "| %2d ", table_representation.t[i]);
    }
    printf("|\n\n");
}
