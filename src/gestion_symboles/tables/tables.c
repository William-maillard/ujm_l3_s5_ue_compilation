/* **********************************************************************
 * Definition des fonctions concernant les tables du compilateur CPYRR  *
 *                                                                      *
 *    -table lexicographique                                            *
 *    -table des declarations                                           *
 *    -table de representation des types et des entetes de sous         *
 *     programme                                                        *
 *    -table des regions                                                *
 *                                                                      *
 *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/tables.h"


/**** Initialise toutes les tables ****/
void init_tables(){
    init_table_lexico();
    init_table_declarations();
    init_table_representations();
}

/**** Affiche toutes les tables ****/
void affiche_tables(){
    affiche_table_lexico();
    affiche_table_declarations();
    affiche_table_representations();
    affiche_table_regions();
}