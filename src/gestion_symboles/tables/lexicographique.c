/******************************************************************
 *                                                                *
 * Definition des fonctions concernant la table lexicographique : *
 *                                                                *
 *     - Initialisation                                           *
 *     - Obtenir le numero lexicographique                        *
 *       (insere le lexeme si pas present)                        *
 *     - Obtenir le lexeme d'un num_lexico                        *
 *     - Affichage                                                *
 *                                                                *
 ******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../../inc/tables.h"
#include "../../../inc/y.tab.h"

// globals variables
struct_table_lexicographique table_lexicographique;

/* ***
 * Initialise la table lexicographique
 *** */
void init_table_lexico()
{
    int i;

    // -1 pour la table des hash_code
    memset(&(table_lexicographique.hash_code), -1, LONGUEUR_TAB_HASH_CODE * sizeof(int));

    /*
       Les 4 premiers indices de enregistrement correspondent a des types simples
    */
    table_lexicographique.enregistrement[0] = (infos_lexicographiques){3, "int", -1};
    table_lexicographique.enregistrement[1] = (infos_lexicographiques){5, "float", -1};
    table_lexicographique.enregistrement[2] = (infos_lexicographiques){4, "char", -1};
    table_lexicographique.enregistrement[3] = (infos_lexicographiques){4, "bool", -1};

    for (i = 4; i < LONGUEUR_TAB_LEXICO; i++)
    {
        table_lexicographique.enregistrement[i] = (infos_lexicographiques){-1, NULL, -1};
    }
    table_lexicographique.taille = 4;
}

/* -- fonction de hash qui associe à chaque lexème un entier positif et le retourne --*/
// -- n est la taille du lexeme '\0' exclus.
static int hash(char *lexeme, int n)
{
    int i, hash = 0;

    for (i = 0; i <= n; i++)
    {
        hash += (int)lexeme[i];
    }

    return hash % LONGUEUR_TAB_HASH_CODE;
}

/****
 * insere lexeme dans la table lexicographique s'il n'est pas présent
 * et retourne son numero lexicographique
 * lexeme -> chaine representant le nom de la variable | la valeur de la constante reel
 ****/
int inserer_lexico(char *lexeme)
{
    int taille = strlen(lexeme);
    int num_lexico, precedent = -1;
    int hcode;
    int existe = 0;
    char *copie_lexeme;

    /* -----
       Creation de son numero lexicographique
       ----- */

    // on recupere son hash et on regarde si un lexeme de meme hash existe deja
    hcode = hash(lexeme, taille);
    precedent = table_lexicographique.hash_code[hcode];

    /*
      si au moins un lexeme de même hash est déja présent dans la table lexicographique
      alors il faut :
      - verifier que ce n'est pas le meme
      - parcourir le chainage donnee par le champs suivant
      - et repeter ces deux etapes jusqua trouver le lexeme ou atteindre la fin du chainage
    */
    if (precedent != -1)
    {
        do
        {
            num_lexico = precedent;
            if (taille == table_lexicographique.enregistrement[num_lexico].longueur &&
                strcmp(lexeme, table_lexicographique.enregistrement[num_lexico].lexeme) == 0)
            {
                existe = 1;
            }
            else
            {
                precedent = table_lexicographique.enregistrement[num_lexico].suivant;
            }
        } while (precedent != -1 && !existe);

        // on conserve le num lexico du dernier lexeme du chainage dans precedent
        precedent = num_lexico;
    }
    if (!existe)
    {
        // recuperation du numero lexicographique = l'indice libre dans la table enregistrement
        num_lexico = table_lexicographique.taille;

        if (num_lexico == LONGUEUR_TAB_LEXICO)
        {
            fprintf(stderr, "Erreur, plus de place dans la table lexicographique (%d lexemes).\nFin de la compilation, utilisez momins de lexemes dans votre programme !\n", LONGUEUR_TAB_LEXICO);
            exit(-1);
        }

        if (precedent == -1)
        {
            // on insere son numero lexicographique dans la table des hash_code
            table_lexicographique.hash_code[hcode] = num_lexico;
        }
        else
        {
            // on fait 'pointer' precedent sur le lexeme a inserer
            table_lexicographique.enregistrement[precedent].suivant = num_lexico;
        }

        /* -----
           Insertion du lexeme
           ----- */

        table_lexicographique.enregistrement[num_lexico].longueur = taille;
        copie_lexeme = strdup(lexeme);
        if (copie_lexeme == NULL)
        {
            fprintf(stderr, "Erreur de malloc (fct inserer_lexico) ! Fin du programme.\n");
            exit(-1);
        }
        table_lexicographique.enregistrement[num_lexico].lexeme = copie_lexeme;
        table_lexicographique.enregistrement[num_lexico].suivant = -1;
        table_lexicographique.taille++;
    }

    return num_lexico;
}

/* ***
 * retourne le lexeme correspondant au numero lexicographique
 * passe en parametre. NULL si pas de lexeme correspondant
 *** */
char *lexeme(int numero_lexicographique)
{
    return table_lexicographique.enregistrement[numero_lexicographique].lexeme;
}

/* ***
 * Affiche la table lexicographique
 *** */
void affiche_table_lexico()
{
    int i;

    printf("/=============TABLE LEXICOGRAPHIQUE============\\\n");
    printf("| Hash|      Table enregistrement              |\n");
    printf("|     |  i  | long |     lexeme      | suivant |\n");
    for (i = 0; i < LONGUEUR_TAB_LEXICO; i++)
    {
        if (table_lexicographique.enregistrement[i].longueur != -1)
        {
            if (i < LONGUEUR_TAB_HASH_CODE)
                printf("| %3d |", table_lexicographique.hash_code[i]);
            else
                printf("      |");

            printf(" %3d |   %2d | %15s | %7d |\n", i,
                   table_lexicographique.enregistrement[i].longueur,
                   table_lexicographique.enregistrement[i].lexeme,
                   table_lexicographique.enregistrement[i].suivant);
        }
    }
    printf("================================================\n");
}
