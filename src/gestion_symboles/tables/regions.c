/**************************************************************
 *                                                            *
 * Definition des fonctions concernant la table des regions : *
 *                                                            *
 *     - Insertion d'une nouvelle region                      *
 *     - Affichage                                            *
 *     - insere l'arbre d'une region                          *
 *                                                            *
 **************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "../../../inc/tables.h"
#include "../../../inc/pile_region.h"

extern int nb_regions;

// globals variables
infos_regions table_regions[LONGUEUR_TAB_REGION];
int pile_region[TAILLE_MAX_PILE]; // la taille est stocke a l'indice 0

/****
 * Fonction d'ajout des infos a la creation d'une region
 *** */
void inserer_region()
{
    table_regions[region_courante()] =
        (infos_regions){TAILLE_CHAINAGE, nis_region_courante(), NULL};
}

/****
 * Fonction d'affichage de la table des regions.
 *** */
void affiche_table_regions()
{
    int i;

    printf("/=====TABLE REGIONS=====\\\n");
    printf("|taille |  NIS  | arbre |\n");
    for (i = 0; i <= nb_regions; i++)
    {
        printf("|  %3d  |  %2d   |  %p    |\n",
               table_regions[i].taille,
               table_regions[i].nis,
               table_regions[i].arbre);
    }
    printf("\\=======================/\n\n");
}

/****
 * Insere l'abre de la region courante a dans la table
 * des regions.
 ****/
void inserer_arbre_region(arbre a)
{
    table_regions[region_courante()].arbre = a;
}

/****
 * Libere la memoire occupe par les
 * differentes regions du programme.
 ****/
void libere_mem_regions()
{
    int i;
    for (i = 0; i <= nb_regions; i++)
    {
        detruire_arbre(table_regions[i].arbre);
    }
}