CC=emcc
CFLAGS= -sWASM=1 -sEXPORTED_FUNCTIONS="['*']"

# Répertoire racine du projet
ROOT_DIR=/src/

# Répertoires sources et objets
SRC_DIR=$(ROOT_DIR)/src
BUILD_DIR=$(ROOT_DIR)/build/wasm

# Liste des répertoires de sources
SUB_DIRS=compilation compilation/semantique \
		execution execution/VM \
		gestion_symboles gestion_symboles/tables \
		src_generes

# Liste des fichiers sources (avec les chemins complets)
SOURCES=$(foreach dir,$(SUB_DIRS),$(wildcard $(SRC_DIR)/$(dir)/*.c))

# Génération des noms de fichiers objets (avec les chemins complets)
WASM = $(patsubst $(SRC_DIR)/%.c,$(BUILD_DIR)/%.js,$(SOURCES))

# dependances:
# 	apt update
# 	apt install flex
# 	apt install bison

all: $(WASM)

# Règle générique pour la compilation des fichiers objets
$(BUILD_DIR)/%.js: $(SRC_DIR)/%.c
	$(CC) $< $(CFLAGS) -o $@