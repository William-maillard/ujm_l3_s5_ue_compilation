#ifndef _PILE_EXECUTION_H_
#define _PILE_EXECUTION_H_

#include "arbres.h"
#include "pile_region.h" // macro NIS_MAX

#define TAILLE_PILE_EXECUTION 5000
#define TAILLE_TAB_BASES 50

typedef union
{
  int entier;
  float reel;
  int booleen;
  char caractere;
} cellule;

extern cellule pile[TAILLE_PILE_EXECUTION];

// stocke temporairement la valeur retourne par une fonction.
extern cellule valeur_retourne;

/* on utilise un tableau pour conserver les BC.
   Dans la pile on stocke l'indice dans ce tableau
   au lieu de tous le chainage, pour eviter la
   redondance d'informations.

   NB : cette indice equivaut au NIS de la region
   courante.
 */

extern int chainage_statique[NIS_MAX];

/* L'indice de la base courante */
extern int BC;
/* Lindice de la base courante si on empile une autre region */
extern int prochaine_base;

/****
 * Depile une la zone memoire de la
 * region courante
 ****/
void depiler();

/****
 * Effectue dans la pile tous les changements relatifs au
 * changement de region que sont
 *  - Mise a jour du chainage dynamique
 *  - Mise a jour des chainages statiques
 *  - Changement de base courante
 ****/
void empiler(int num_region);

/****
 * Calcule le deplacement pour acceder a un champ,
 * dans une structure.
 * Entree : - num declaration de la structure
 *          - pointeur sur un noeud A_IDF representant un champ d'une structure
 * Sortie : le deplacement
 ****/
int recherche_indice_champ(int num_decl, arbre a);

/****
 * Calcule le deplacement pour acceder a une case
 * (indique par une liste d'indices) d'un tableau,
 * en partant de sa borne inferieur.
 * Entree : - num declaration du tableau
 *          - pointeur sur un noeud A_LISTE_INDICES
 * Sortie : le deplacement
 ****/
int recherche_indice_tableau(int num_decl, arbre a);

/****
 * Recupere l'indice d'une variable dans
 * la pile d'execution.
 * Entree : un pointeur sur un noeud A_{MOINS_}VARIABLE
 * Sortie : l'indice dans la pile correspondant a cette
 *          variable
 ****/
int recuperer_indice_pile(arbre a);

/****
 * Ins�re une valeur dans la pile
 ****/
void inserer_valeur_pile(int indice, cellule valeur);

/****
 * Met a jour les informations de la pile
 * (cf empiler)
 * et stocke les valeurs des params dans la pile.
 * Retourne le numero de la nouvelle region courante.
 ****/
int changer_region(arbre a);

/****
 * Affiche la pile d'execution
 * sur le terminal.
 * Attention affiche que des entier
 * affichage indeterminer pour
 * les autres types stockes dans
 * la pile.
 ****/
void affiche_pile();
#endif