#ifndef _SAUVEGARDE_H_
#define _SAUVEGARDE_H_

/****
 * Sauvegarde les arbres des region
 * et les tables d'un programme
 * CPYRR necessaires pour executer,
 * ulterrieurement, un programme
 ****/
void sauvegarde_programme(FILE *fichier);

#endif