/* **********************************************************************
 * Declaration des tables utilisées par le compilateur CPYRR et des     *
 *  des fonctions concernant ces tables :                               *
 *                                                                      *
 *    -table lexicographique                                            *
 *    -table des declarations                                           *
 *    -table de representation des types et des entetes de sous         *
 *     programme                                                        *
 *    -table des regions                                                *
 *    -fonctions d'initialisation/affichage de toutes les tables        *
 *                                                                      *
 *********************************************************************** */
#ifndef _TABLES_H_
#define _TABLES_H_
#include "arbres.h"

#define LONGUEUR_TAB_HASH_CODE 32
#define LONGUEUR_TAB_LEXICO 500
#define DEBORDEMENT_TAB_DECLARATION 4500
#define LONGUEUR_TAB_DECLARATION LONGUEUR_TAB_LEXICO + DEBORDEMENT_TAB_DECLARATION
#define LONGUEUR_TAB_REP_TYPE_ET_ENTETE_SOUS_PROG 1000
#define LONGUEUR_TAB_REGION 50

#define LONGUEUR_LEXEME_MAX 50
#define LONGUEUR_CONSTANTE_CHAINE_MAX 300

#define TAILLE_CHAINAGE 2 // taille du chainage dynamique + statique

#define NUM_DEC_VIDE -2
#define NUM_DEC_ENTIER 0
#define NUM_DEC_REEL 1
#define NUM_DEC_CARACTERE 2
#define NUM_DEC_BOOLEEN 3
#define NUM_DEC_CHAINE 4

/*  -------------------------------
   |                               |
   |     table lexicographique     |
   |                               |
    ------------------------------- */

typedef struct
{
    int longueur;
    char *lexeme;
    int suivant;
} infos_lexicographiques;

typedef struct
{
    int hash_code[LONGUEUR_TAB_HASH_CODE];
    infos_lexicographiques enregistrement[LONGUEUR_TAB_LEXICO];
    int taille;
} struct_table_lexicographique;

extern struct_table_lexicographique table_lexicographique;

/* ***
 * Initialise la table lexicographique
 *** */
void init_table_lexico();

/* ***
 * insere lexeme dans la table lexicographique s'il n'est pas present
 * et retourne son numer lexicographique
 * ***
 * lexeme -> nom de la variable / valeur de la constante reel
 ***  */
int inserer_lexico(char *lexeme);

/* ***
 * retourne le lexeme correspondant au numero lexicographique
 * passe en parametre
 *** */
char *lexeme(int numero_lexicographique);

/* ***
 * Affiche la table lexicographique
 *** */
void affiche_table_lexico();

/*  -------------------------------
   |                               |
   |     table des declarations    |
   |                               |
    ------------------------------- */

typedef enum
{
    TYPE_BASE = 2,
    TYPE_STRUCTURE = -3,
    TYPE_TABLEAU = 3,
    TYPE_VARIABLE = 5,
    TYPE_PROCEDURE = -7,
    TYPE_FONCTION = 7
} nature;

typedef struct
{
    nature nature;
    int suivant;
    int region;
    int description;
    int execution;
} infos_declaration;

typedef struct
{
    infos_declaration t[LONGUEUR_TAB_DECLARATION];
    int debordement; // indice libre dans la partie debordement de la table
} structure_dec;
// Il va y avoir une fragmentation interne -> les indices correspondant a un num_lexico
// d'un champ/parametre vont etre vide.

extern structure_dec table_declarations;

/* ***
 * Retourne le numero de declaration associe au non de variable passe en parametre.
 * En prenant en compte les variables globales/locales definies
 * -1 si pas de lexeme de nature nat accessible depuis la region courante
 *** */
int association_nom(int num_lexico, nature nat);

/****
 * Initialise la table des declarations
 *** */
void init_table_declarations();

/*****
 * insertion dans la table des déclarations :
 **** */
int inserer_dec(nature nat, int num_lexico);

/* ***
 * Remplis la colonnes descriptions  pour une variable
 * et ajoute l'espace memoire occupe par la variable a la region courante
 *** */
void completer_dec_variable(int num_dec, int num_dec_type);

/****
 * Affiche la table des declarations
 *** */
void affiche_table_declarations();

/* -------------------------------
  |                               |
  |    table de representation    |
  |    des types et des entetes   |
  |    de sous-programmes         |
  |                               |
   ------------------------------- */
typedef struct
{
    int t[LONGUEUR_TAB_REP_TYPE_ET_ENTETE_SOUS_PROG];
    int indice_libre;
} structure_rep;

extern structure_rep table_representation;

/****
 * Initialisation de la table des representations
 *** */
void init_table_representations();

/* ***
 * Ajoute un champs a la structure dont l'indice
 * dans la table des declarations est passe en parametre.
 * Chaque champs prend 3 cases dans la table de representation :
 *   - numero lexico
 *   - indice declaration de son type
 *   -  deplacement par rapport au debut de la structure pour atteindre le champs.
 * Retourne -1 si un champs du meme nom existe deja.
 *** */
int ajouter_champs(int num_lexico_champs, int num_dec_type, int num_dec_struct);

/* ***
 * ajoute une dimension au tableau
 * Pour l'instant on ne gere PAS les dim donnees sous forme d'expression
 *** */
void ajouter_dimension(int borne_inf, int borne_supp, int num_dec_tab);

/* ***
 * Appele a la fin de la declaration d'un tableau,
 * Permet d'ajouter le type des elements d'un tableau
 * et sa taille dans la colonne execution de la table des declarations
 *** */
void inserer_type_taille_tableau(int num_dec_tab, int num_dec_type);

/****
 * Ajoute le type de retour dans la table des representations,
 * a la fonction/procedure dont le num de representation est passe en parametre
 *** */
void inserer_type_retour(int num_rep_fct, int num_dec_type);

/****
 * Insere un parametre dans les tables :
 *   - le numero lexico de son lexeme
 *   - sa declaration dans la tab des declaration
 *   - son num_lexico|num_dec|num_dec_type dans la table des declarations
 * a la fonction/procedure dont le num de declaration est passe en parametre
 *** */
void ajouter_parametre(int num_rep_fct, int num_lexico, int num_dec_type);

/****
 * Affiche la table des representations
 *** */
void affiche_table_representations();

/* -------------------------------
  |                               |
  |       table des regions       |
  |                               |
   ------------------------------- */
typedef struct
{
    int taille, nis;
    arbre arbre;
} infos_regions;

extern infos_regions table_regions[LONGUEUR_TAB_REGION];

// pour la taille d'une region il faut l'augmenter a chaque variable declare
// (en ajoutant la taille de la nature de la variable)
// le champs arbre sera remplis dans le YACC
// avec le NIS (=taille de la pile lorsque l'on rentre/sort de la region)

/****
 * Fonction d'initialisation
 *** */
void inserer_region();

/****
 * Fonction d'affichage de la table des regions
 *** */
void affiche_table_regions();

/****
 * Fonction d'affichage de la table des regions
 *** */
void affiche_table_regions();

/****
 * Insere l'abre de la region courante a dans la table
 * des regions.
 ****/
void inserer_arbre_region(arbre a);

/****
 * Libere la memoire occupe par les
 * differentes regions du programme.
 ****/
void libere_mem_regions();

/* -------------------------------
  |                               |
  |     Fonctions generales       |
  |                               |
   ------------------------------- */

/**** Initialise toutes les tables ****/
void init_tables();

/**** Affiche toutes les tables ****/
void affiche_tables();

#endif