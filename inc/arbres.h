/*************************************
 *                                   *
 * Fonctions Pour creer              *
 * et manipuler des arbres binaires  *
 * Pere : Fils - Frere               *
 *                                   *
 *************************************/
#ifndef _ARBRE_H_
#define _ARBRE_H_

typedef struct noeud
{
    int nature, valeur;
    struct noeud *fils, *frere;
} noeud;

typedef struct noeud *arbre;

typedef enum
{
    A_PROG,
    A_CORPS,
    A_LISTE_INSTRUCTION,
    A_RETOURNE,
    A_LISTE_PARAM,
    A_INST_COND,
    A_TANT_QUE,
    A_AFFECTATION,
    A_LISTE_INDICES,
    A_IDF,
    A_VARIABLE,
    A_CSTE_ENTIERE,
    A_CSTE_REEL,
    A_CSTE_BOOLEEN,
    A_CSTE_CARACTERE,
    A_CSTE_CHAINE,
    A_EXPRESSION_ARITHMETIQUE,
    A_EXPRESSION_BOOLEENNE,
    A_EXPRESSION_RELATIONNELLE,
    A_PLUS,
    A_MOINS,
    A_MULT,
    A_DIV,
    A_MOD,
    A_OU,
    A_ET,
    A_NON,
    A_INF,
    A_INF_EGAL,
    A_SUP,
    A_SUP_EGAL,
    A_EGAL,
    A_DIFFERENT,
    A_LIRE,
    A_ECRIRE,
    A_APPEL_FCT,
    A_APPEL_PROC,
    A_ENTREE,
    A_SORTIE,
    A_LISTE_VARIABLES
} type_noeud;

/****
 * Cree un arbre vide
 **** */
arbre arbre_vide();

/****
 * Créer un noeud et renvoie le pointeur sur ce noeud.
 **** */
arbre creer_noeud(int nature, int valeur);

/****
 * Ajoute fils au noeud pere et renvoie un pointeur sur le père.
 **** */
arbre concat_pere_fils(arbre pere, arbre fils);

/* *****
 * Ajoute frere au noeud pere et renvoie un pointeur sur le pere.
 ***** */
arbre concat_pere_frere(arbre pere, arbre frere);

/* *****
 * Supprime un arbre et libère la mémoire occupée.
 * renvoie le pointeur NULL
 ***** */
arbre detruire_arbre(arbre a);

/* *****
 * Affiche l'arbre a sur la sortie standard.
 ***** */
void affiche_arbre(arbre a);

#endif