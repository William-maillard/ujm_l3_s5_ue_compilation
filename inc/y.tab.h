/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_INC_Y_TAB_H_INCLUDED
# define YY_YY_INC_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    PROG = 258,                    /* PROG  */
    DEBUT = 259,                   /* DEBUT  */
    FIN = 260,                     /* FIN  */
    RETOURNE = 261,                /* RETOURNE  */
    IDF = 262,                     /* IDF  */
    LIRE = 263,                    /* LIRE  */
    ECRIRE = 264,                  /* ECRIRE  */
    TYPEDEF = 265,                 /* TYPEDEF  */
    STRUCT = 266,                  /* STRUCT  */
    VARIABLE = 267,                /* VARIABLE  */
    TABLEAU = 268,                 /* TABLEAU  */
    DE = 269,                      /* DE  */
    PROCEDURE = 270,               /* PROCEDURE  */
    FONCTION = 271,                /* FONCTION  */
    VIDE = 272,                    /* VIDE  */
    ENTIER = 273,                  /* ENTIER  */
    REEL = 274,                    /* REEL  */
    BOOLEEN = 275,                 /* BOOLEEN  */
    CARACTERE = 276,               /* CARACTERE  */
    CSTE_ENTIERE = 277,            /* CSTE_ENTIERE  */
    CSTE_BOOLEEN = 278,            /* CSTE_BOOLEEN  */
    CSTE_CARACTERE = 279,          /* CSTE_CARACTERE  */
    CSTE_REEL = 280,               /* CSTE_REEL  */
    CSTE_CHAINE = 281,             /* CSTE_CHAINE  */
    OPAFF = 282,                   /* OPAFF  */
    PLUS = 283,                    /* PLUS  */
    MOINS = 284,                   /* MOINS  */
    MULT = 285,                    /* MULT  */
    DIV = 286,                     /* DIV  */
    MOD = 287,                     /* MOD  */
    EGAL = 288,                    /* EGAL  */
    DIFFERENT = 289,               /* DIFFERENT  */
    SUPERIEUR = 290,               /* SUPERIEUR  */
    SUP_EGAL = 291,                /* SUP_EGAL  */
    INFERIEUR = 292,               /* INFERIEUR  */
    INF_EGAL = 293,                /* INF_EGAL  */
    OU = 294,                      /* OU  */
    ET = 295,                      /* ET  */
    NON = 296,                     /* NON  */
    TANT_QUE = 297,                /* TANT_QUE  */
    SI = 298,                      /* SI  */
    ALORS = 299,                   /* ALORS  */
    SINON = 300,                   /* SINON  */
    VIRGULE = 301,                 /* VIRGULE  */
    POINT_VIRGULE = 302,           /* POINT_VIRGULE  */
    POINT = 303,                   /* POINT  */
    POINT_POINT = 304,             /* POINT_POINT  */
    DEUX_POINTS = 305,             /* DEUX_POINTS  */
    PARENTHESE_OUVRANTE = 306,     /* PARENTHESE_OUVRANTE  */
    PARENTHESE_FERMANTE = 307,     /* PARENTHESE_FERMANTE  */
    CROCHET_OUVRANT = 308,         /* CROCHET_OUVRANT  */
    CROCHET_FERMANT = 309,         /* CROCHET_FERMANT  */
    ACCOLLADE_OUVRANTE = 310,      /* ACCOLLADE_OUVRANTE  */
    ACCOLLADE_FERMANTE = 311       /* ACCOLLADE_FERMANTE  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define PROG 258
#define DEBUT 259
#define FIN 260
#define RETOURNE 261
#define IDF 262
#define LIRE 263
#define ECRIRE 264
#define TYPEDEF 265
#define STRUCT 266
#define VARIABLE 267
#define TABLEAU 268
#define DE 269
#define PROCEDURE 270
#define FONCTION 271
#define VIDE 272
#define ENTIER 273
#define REEL 274
#define BOOLEEN 275
#define CARACTERE 276
#define CSTE_ENTIERE 277
#define CSTE_BOOLEEN 278
#define CSTE_CARACTERE 279
#define CSTE_REEL 280
#define CSTE_CHAINE 281
#define OPAFF 282
#define PLUS 283
#define MOINS 284
#define MULT 285
#define DIV 286
#define MOD 287
#define EGAL 288
#define DIFFERENT 289
#define SUPERIEUR 290
#define SUP_EGAL 291
#define INFERIEUR 292
#define INF_EGAL 293
#define OU 294
#define ET 295
#define NON 296
#define TANT_QUE 297
#define SI 298
#define ALORS 299
#define SINON 300
#define VIRGULE 301
#define POINT_VIRGULE 302
#define POINT 303
#define POINT_POINT 304
#define DEUX_POINTS 305
#define PARENTHESE_OUVRANTE 306
#define PARENTHESE_FERMANTE 307
#define CROCHET_OUVRANT 308
#define CROCHET_FERMANT 309
#define ACCOLLADE_OUVRANTE 310
#define ACCOLLADE_FERMANTE 311

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 62 "src/compilation/analyseYacc.y"

    int entier;
    char *chaine;
    arbre arbre;
 

#line 186 "inc/y.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_INC_Y_TAB_H_INCLUDED  */
