/********************************************************
 * Module implementant les fonctions relatives a la     *
 * machine vituelle :                                   *
   - execution de l'arbre                               *
   - evaluation de l'arbre                              *
*********************************************************/

#ifndef _MACHINE_VIRTUELLE_H_
#define _MACHINE_VIRTUELLE_H_

#include "pile_execution.h"

extern int flag_pile; // pour opt du prog.

/****
 * Lance la machine virtuelle
 ****/
void machine_virtuelle(arbre a);

/****
 * Execute les instructions de l'arbre donne en parametre
 * Entree : arbre d'une instruction
 ****/
void execute_arbre(arbre a);

/****
 * Evalue l'arbre d'une expression arithmetique sur des entiers
 ****/
int evalue_arbre_int(arbre a);

/****
 * Evalue l'arbre d'une expression arithmetique sur des reels
 ****/
float evalue_arbre_float(arbre a);

/****
 * Evalue l'arbre d'une expression relationnelle
 * Entree : l'arbre d'une expression relationnelle
 * Sortie : - 1 si l'exp est evalue a VRAIE
 *          - 0 sinon (FAUX)
 ****/
int evalue_arbre_relationnel(arbre a);

/****
 * Evalue l'arbre d'une expression booléenne.
 * Entree : pointeur sur le noeud d'une operation booleenne
 * Sortie : 1 si vraie, 0 sinon.
 ****/
int evalue_arbre_booleen(arbre a);

/****
 * Evalue L'arbre d'une valeur.
 * Entree : - arbre d'une cste
 *          - arbre d'une variable
 *          - arbre d'une fonction
 *          - arbre d'une expression
 * Sortie : une cellule contenant la valeur
 ****/
cellule evalue_valeur(arbre a);

void executer_ecrire(arbre a);
void executer_lire(arbre a);

#endif