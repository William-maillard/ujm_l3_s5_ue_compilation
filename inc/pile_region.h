/*****************************************************************
 *                                                               *
 * Définition de la pile des regions utilise pour la compilation *
 * et l execution d un programme CPYRR.                          *
 * Et des fonctions permettant de manipuler cette pile           *
 * Pour chaque region on a :                                     *
 *   - le num de declaration du type retourne                    *
 *   - le numero de la region                                    *
 *                                                               *
 **************************************************************** */
#ifndef _PILE_REGION_H_
#define _PILE_REGION_H_

#define NIS_MAX 10 // Niveau d'Imbrication Static max
#define TAILLE_MAX_PILE NIS_MAX * 2 + 1

/*  ------------------------------
   |                              |
   |       Pile des regions       |
   |                              |
    ------------------------------ */

extern int pile_region[TAILLE_MAX_PILE]; // la taille est stocke a l'indice 0

/* *** Initialise la pile *** */
void init_pile_region();

/* *** renvoie le nis de la region en sommet de pile *** */
int nis_region_courante();

/* *** renvoie le numero de la region courante *** */
int region_courante();

/* ***
 * renvoie le numero de declaration
 * du type retourne par la region courante
 *** */
int type_retour();

/* *** sort de la region courante *** */
void depiler_region();

/* *** region devient la region courante *** */
void empiler_region(int region);

/* *** ajout un type de retour a la region courante *** */
void empiler_type_retour(int type_retour);

/* ***
 * cherche a quel niveau se situe region par rapport a la region courante.
 * retourne -1 si region n'est pas une region qui englobe la region courante
 * son niveau d'englobement sinon.
 * (ex si region = region precedente de la region courante -> 1
 *          region precedent la precedente ______________  -> 2
 * ...)
 *** */
int region_englobante(int region);

/* *** Renvoie le numero de la i° region englobante *** */
int num_region_englobante(int i);

/* *** affiche la pile des regions sur le terminal *** */
void affiche_pile_region();

#endif