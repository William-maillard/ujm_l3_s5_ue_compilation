#ifndef _ANALYSE_SEMANTIQUE_H_
#define _ANALYSE_SEMANTIQUE_H_
#include "arbres.h"

/* ---------------------------------
  |                                 |
  |            VARIABLE             |
  |                                 |
   --------------------------------- */
/****
 * Verifie la semantique d'une variable :
 * - si elle est declare
 * - si les champs eventuels existe
 * - si on donne des dimensions qu'a un type tableau
 * retourne le num de declaration du type de la variable
 * Entree : pointeur sur un noeud A_VARIABLE
 * Sortie : le num_dec du type de la variable si ok,
 *          CODE_ERREUR sinon.
 ****/
int semantique_variable(arbre a_variable);

/* ---------------------------------
  |                                 |
  |          AFFECTATION            |
  |                                 |
   --------------------------------- */
/****
 * Pour les messages d'erreur, permet d'afficher
 * sous forme de chaine de caractere les types
 * simples du langage CPYRR a partir de leur
 * macro donnee en parametre
 ****/
char *type_simple_chaine(int n);

/****
 * Verifie la semantique d'une valeur.
 * Appel les fonctions de semantiques concerne suivant
 * la valeur
 * Entree : un arbre d'une valeur
 * Sortie : num dec du type de la valeur, CODE_ERREUR sinon
 ****/
int semantique_valeur(arbre a);

/****
 * Verifie que la partie droite est une variable valide,
 * et que la partie gauche est une valeur valide de
 * meme type.
 * Entree :  un pointeur sur un noeud A_AFFECTATION
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_affectation(arbre a);

/* ---------------------------------
  |                                 |
  |           EXPRESION             |
  |                                 |
   --------------------------------- */
/****
 * Verifie la semantique d'une expression arithmetique,
 * Entree : l'abre d'une operation arithmetique
 * Sortie : le num dec du type du resultat de cette expression
 * (entier ou reel), CODE_ERREUR sinon
 ****/
int semantique_expression_arithmetique(arbre a);

/****
 * Verifie la semantique d'un expression relationnelle,
 * les operandes doivente etre de meme type (entier ou reel)
 * Entree : l'arbre d'une operation relationnelle
 * Sortie : NUM_DEC_{ENTIER, REEL} si ok, CODE_ERREUR sinon
 ****/
int semantique_expression_relationnelle(arbre a);

/****
 * Verifie la semantique d'une expression boolenne,
 * les operande doivent etre des booleens.
 * Entree : l'arbre d'une operation booleenne
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_expression_booleenne(arbre a);

/* ---------------------------------
  |                                 |
  |             APPEL               |
  |                                 |
   --------------------------------- */
/****
 * Verifie la semantique d'un appel :
 *   - nombre de prametres
 *   - type de chaque parametre
 * Entree : l'arbre d'un appel
 * Sortie : le type de retour si ok, CODE_ERREUR sinon
 ****/
int semantique_appel(arbre a);

/****
 * Verifie que la valeur retourne correspond
 * au type de retour de la fonction courante
 * Entree :  pointeur sur un noeud A_RETOURNE
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_retourne(arbre a);

/****
 * Verifie qu'une fonction qui retourne une valeur
 * se termine par un return.
 * Entree : arbre d'un block d'instruction
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_corps_fonction(arbre a);

/* ---------------------------------
  |                                 |
  |       LECTURE ECRITURE          |
  |                                 |
   --------------------------------- */
/****
 * Verifie la correspondance entre les formats d'une chaine formatee
 * et la liste de valeurs correspondante.
 * Entree : l'arbre lire ou ecrire
 * Sortie : CODE_VALIDE si ok, CODE_ERREUR sinon
 ****/
int semantique_chaine_formatee(arbre a);

/* ---------------------------------
  |                                 |
  |          INSTRUCTION            |
  |                                 |
   --------------------------------- */
/****
 * Verifie la semantique d'une instruction
 * (et donc recursivement de tous les elements
 *  qui la composent)
 * Entree : un arbre d'un instruction
 * Sortie : CODE_VALIDE si ok sinon CODE_ERREUR
 ****/
// int semantique_instruction(arbre a);
/*
  NB : n'est plus utilise, car avant '\n' etaut lus avant
  l'analyse semantique (i.e buffer ligne contiet la ligne d'apres)
  Ce qui n'etais pas pratique pour les affichages d'erreurs.
*/

#endif