# Projet Compilation 2021-2022
CC = gcc
CFLAGS = -Wall -O3

ANALYSE_COMPILATION = obj/analyse_syntaxique.o obj/analyse_lexicale.o 
ANALYSE_CHARGEMENT = obj/analyse_syntaxique_chargement.o obj/analyse_lexicale_chargement.o

OBJETS =  obj/pile_region.o obj/arbres.o
TABLES = obj/declarations.o obj/lexicographique.o obj/regions.o obj/representations.o obj/tables.o
SEMANTIQUE = obj/sem_variable.o obj/sem_affectation.o obj/sem_expression.o obj/sem_appel.o obj/sem_lecture_ecriture.o
EXECUTION = obj/machine_virtuelle.o obj/pile_execution.o obj/evaluation_expression.o obj/evaluation_valeur.o obj/execute_lire_ecrire.o


all : analyse machine_virtuelle

# -------------- Creation des executables --------------#

analyse : $(ANALYSE_COMPILATION)  $(TABLES) $(OBJETS) $(SEMANTIQUE) obj/sauvegarde.o $(EXECUTION)
	$(CC) -o bin/$@ $(CFLAGS) $^

#creation de l'executable
machine_virtuelle :  $(ANALYSE_CHARGEMENT) $(TABLES) $(OBJETS) $(EXECUTION) 
	$(CC) -o bin/$@ $(CFLAGS) $^


# ----------------------------------------------------- #
#                      COMPILATION                      #
# ----------------------------------------------------- #


# -------- Creation des source et objets des analyseurs --------- #

# options :
#    -d genere un fichier .h
#    --defines[=FILE] pour renommer le fichier .h
#    -v genere y.output.c --> voir les eventuels conflits
obj/analyse_syntaxique.o : src/compilation/analyseYacc.y
	yacc  -o src/src_generes/analyse_syntaxique.tab.c --defines="inc/y.tab.h" -v $<
	cp inc/y.tab.h src/src_generes/y.tab.h
	$(CC) -o $@ $(CFLAGS) -c src/src_generes/analyse_syntaxique.tab.c

obj/analyse_lexicale.o : src/compilation/analyseLex.l inc/y.tab.h
	lex -o src/src_generes/analyse_lexicale.yy.c  $<
	$(CC) -o $@ $(CFLAGS) -c src/src_generes/analyse_lexicale.yy.c


# ----------- Creation des fichiers objets ------------ #

# Les tables :
obj/tables.o : src/gestion_symboles/tables/tables.c inc/tables.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/declarations.o : src/gestion_symboles/tables/declarations.c inc/tables.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/lexicographique.o : src/gestion_symboles/tables/lexicographique.c inc/tables.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/regions.o : src/gestion_symboles/tables/regions.c inc/tables.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/representations.o : src/gestion_symboles/tables/representations.c inc/tables.h
	$(CC) -o $@ $(CFLAGS) -c $<

# Arbre :
obj/arbres.o : src/gestion_symboles/arbres.c inc/arbres.h
	$(CC) -o $@ $(CFLAGS) -c $<

# pile region :
obj/pile_region.o : src/compilation/pile_region.c inc/pile_region.h
	$(CC) -o $@ $(CFLAGS) -c $<


# L'analyse semantique :
obj/sem_variable.o : src/compilation/semantique/variable.c inc/analyse_semantique.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/sem_affectation.o : src/compilation/semantique/affectation.c inc/analyse_semantique.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/sem_expression.o : src/compilation/semantique/expression.c inc/analyse_semantique.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/sem_appel.o : src/compilation/semantique/appel.c inc/analyse_semantique.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/sem_lecture_ecriture.o : src/compilation/semantique/lecture_ecriture.c inc/analyse_semantique.h
	$(CC) -o $@ $(CFLAGS) -c $<

#obj/sem_instruction.o : src/compilation/semantique/instruction.c inc/analyse_semantique.h
#	$(CC) -o $@ $(CFLAGS) -c $<

# sauvegarde :
obj/sauvegarde.o: src/compilation/sauvegarde.c
	$(CC) -o $@ $(CFLAGS) -c $<



# ----------------------------------------------------- #
#                       EXECUTION                       #
# ----------------------------------------------------- #


obj/pile_execution.o : src/execution/pile_execution.c inc/pile_execution.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/machine_virtuelle.o : src/execution/VM/machine_virtuelle.c inc/machine_virtuelle.h inc/pile_execution.h
	$(CC) -o $@ $(CFLAGS) -c $<

obj/evaluation_expression.o : src/execution/VM/evaluation_expression.c
	$(CC) -o $@ $(CFLAGS) -c $<

obj/evaluation_valeur.o : src/execution/VM/evaluation_valeur.c
	$(CC) -o $@ $(CFLAGS) -c $<

obj/execute_lire_ecrire.o : src/execution/VM/executer_lire_ecrire.c
	$(CC) -o $@ $(CFLAGS) -c $<


# Chargement (arbre et tables) :
obj/analyse_syntaxique_chargement.o : src/execution/chargement.y inc/arbres.h
	yacc  -o  src/src_generes/analyse_syntaxique_chargement.tab.c --defines="inc/y.chargement.h" $<
	cp inc/y.chargement.h src/src_generes/y.chargement.h
	$(CC) -o $@ $(CFLAGS) -c src/src_generes/analyse_syntaxique_chargement.tab.c

obj/analyse_lexicale_chargement.o : src/execution/chargement.l
	lex -o src/src_generes/analyse_lexicale_chargement.yy.c  $<
	$(CC) -o $@ $(CFLAGS) -c src/src_generes/analyse_lexicale_chargement.yy.c



# ------------- Creation des repertoires ---------------#
repertoires:
	rm -rf obj bin
	mkdir obj bin


# --------------- Regles de supressions --------------- #

# supprime tous les fichiers intermediaires generes
clean :
	rm -f src/src_generes/*.c src/src_generes/*.output obj/*.o

# supprime tous les fichiers genere afin de faire
# une recompilation complete du projet
mrproper : clean
	rm -f  bin/*

