
int getopt(int argc, char * const argv[], const char *optstring);

NB format optstring (3°arguments) :
  - debut de chaine :
      - '+' s'arrete des qu'un argument autre qu'une option est rencontre
      - '-' tous les arguments non options son traite comme argument
        d'une option de code 1
      - ':' retourne ':' au lieu de '?' pour indiquer un argument manquant
        ('?' est renvoye lorsqu'une erreur est rencontree)

  - contient tous les char d'options
  - si un char est suivie de ':' alors il prend un argument, et '::' si cet
    argument est optionnel


variables :
  -'optind' stocke l'indice de argv ou c'est arrete la fct.
  -'optarg' contient le pointeur de argv sur la chaine de l'option 